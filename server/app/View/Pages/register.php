<?php require '../Components/start.php'; ?>
<form action="../../Controller/ControllerRegister.php" class="container" style="max-width: 400px" method="post"
      enctype="multipart/form-data">
    <div class="mb-3">
        <h1>Register to Admin</h1>
        <label for="exampleInputName" class="form-label">Enter you first name</label>
        <input type="text" name="firstName" class="form-control" id="exampleInputName" aria-describedby="nameHelp">
    </div>
    <div class="mb-3">
        <label for="exampleInputLastName" class="form-label">Enter you last name</label>
        <input type="text" name="lastName" class="form-control" id="exampleInputLastName" aria-describedby="nameHelp">
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Email address</label>
        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
        <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
    </div>
    <div class="mb-3">
        <label for="exampleInputPassword1" class="form-label">Password</label>
        <input type="password" name="password" class="form-control" id="exampleInputPassword1">
    </div>
    <div class="mb-3 form-check">
        <input type="checkbox" name="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1">Check me out</label>
    </div>
    <div class="mb-3">
        <label for="submit"></label>
        <input type="submit" class="btn btn-primary" id="submit" value="Submit">
        <label for="javascript"></label>
        <input type="reset" id="javascript" class="btn btn-danger" name="reset" value="Reset" onclick="comment();">
    </div>
</form>
<?php require '../Components/end.php'; ?>