<?php

return [
    'name' => 'iPhone',
    'price' => 99,
    'description' => 'it is good phone',
    'is_active' => 1,
    'created_at' => date('Y-m-d H:i:s'),
    'updated_at' => date('Y-m-d H:i:s'),
    'is_available' => true,
    ];
