<?php
echo 'page of order';

function getSum ($a, $b) {
    return $a + $b;
}
echo getSum(2,2);

function myPrintR(array $variable)
{
    if (is_array($variable)) {
        echo "Array\n(\n";
        foreach ($variable as $key => $value) {
            if (is_array($value)) {
                echo sprintf('    [%s] => ', $key);
                myPrintR($value);
            } else {
                echo sprintf('    [%s] => %d\n', $key, $value);
            }
        }
        echo ")\n";
    } elseif (is_string($variable)) {
        echo $variable . "\n";
    } elseif (is_numeric($variable)) {
        echo $variable . "\n";
    } elseif (is_bool($variable)) {
        echo $variable ? 'true' : 'false';
        echo "\n";
    } elseif (is_null($variable)) {
        echo "NULL\n";
    }
    return $variable;
}