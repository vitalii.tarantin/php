<?php
/**
 *
 */

$options = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
];
$dbh = new PDO(
    'mysql:host=my_mysql;port=3306;dbname=my_data-base;charset=utf8',
    'root',
    'qwerty',
    $options
);

