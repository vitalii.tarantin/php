<?php
/**
 * Homework_03 form
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */
session_start();

require 'list.html';

if ($_REQUEST['submit']) {
    if (!empty($_REQUEST['name'] && $_REQUEST['phone'] && $_REQUEST['postman'] && $_REQUEST['address'])) {
        $name = $_REQUEST['name'];
        $name = trim($name);
        $name = strip_tags($name);
        $name = htmlspecialchars($name);

        $phone = $_REQUEST['phone'];
        $phone = trim($phone);
        $phone = strip_tags($phone);
        $phone = htmlspecialchars($phone);

        $postman = $_REQUEST['postman'];
        $postman = trim($postman);
        $postman = strip_tags($postman);
        $postman = htmlspecialchars($postman);

        $address = $_REQUEST['address'];
        $address = trim($address);
        $address = strip_tags($address);
        $address = htmlspecialchars($address);
    } else {
        echo 'Невсі поля заповненні';
    }
}
