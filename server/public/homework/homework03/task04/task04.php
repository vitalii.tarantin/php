<?php

/**
 * Homework_03 form
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */

session_start();

require 'upload.html';
/**
 * Is function for upload file in server
 *
 * @return void
 */
function upLoadFile()
{
    $valid_types = array('pdf', 'doc', 'xls', 'jpg', 'png');

    if (isset($_FILES['userfile'])) {
        if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {
            $filename = basename($_FILES['userfile']['name']);
            $ext = substr($_FILES['userfile']['name'], 1 + strrpos($_FILES['userfile']['name'], '.'));
            if (in_array($ext, $valid_types)) {
                $uploaddir = '/var/www/html/public/homework/task04/img/';
                $uploadfile = $uploaddir . $filename;
                move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile);
                echo sprintf('<img src="img/%s" alt="%d" title="%c">', $filename, $filename, $filename);
            } else {
                echo 'This file type cannot be downloaded';
            }
        }
    }
}
upLoadFile();
