<?php

class oFile
{
    private $name;
    private $mime;
    private $content;

    public function __construct($name, $mime=null, $content=null)
    {
// Перевіряємо, якщо $content=null, значить у змінній $name - шлях до файлу
        if(is_null($content))
        {
//  Отримуємо інформацію щодо файлу (шлях, ім'я та розширення файлу)
            $info = pathinfo($name);
// перевіряємо чи міститься в рядку ім'я файлу і чи можна прочитати файл
            if(!empty($info['basename']) && is_readable($name))
            {
                $this->name = $info['basename'];
//  Визначаємо MIME тип файлу
                $this->mime = mime_content_type($name);
// Завантажуємо файл
                $content = file_get_contents($name);
// Перевіряємо чи успішно було завантажено файл
                if($content!==false) $this->content = $content;
                else throw new Exception('Don`t get content - "'.$name.'"');
            } else throw new Exception('Error param');
        } else
        {
// Зберігаємо ім'я файла
            $this->name = $name;
// Якщо не було передано тип MIME намагаємося самі його визначити
            if(is_null($mime)) $mime = mime_content_type($name);
// Зберігаємо тип MIME файлу
            $this->mime = $mime;
// Зберігаємо у властивості класу вміст файлу
            $this->content = $content;
        };
    }

// Метод повертає ім'я файлу
    public function Name() { return $this->name; }

// Метод повертає тип MIME
    public function Mime() { return $this->mime; }

// Метод повертає вміст файлу
    public function Content() { return $this->content; }
}