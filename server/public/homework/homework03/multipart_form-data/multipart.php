<?php

// Підключаємо клас-контейнер вмісту файлу
include "ofile.class.php";
// Підключаємо клас для формування тіла POST запиту
include "bodypost.class.php";

// Генеруємо унікальний рядок для розділення частин POST запиту
$delimiter = '-------------'.uniqid();

// Формуємо об'єкт oFile, що містить файл
$file = new oFile('sample.txt', 'text/plain', 'Content file');

// Формуємо тіло POST запиту
$post = BodyPost::Get(array('field'=>'text', 'file'=>$file), $delimiter);

// Ініціалізуємо CURL
$ch = curl_init();

// Вказуємо на який ресурс передаємо файл
curl_setopt($ch, CURLOPT_URL, 'http://server/upload/');
// Вказуємо, що буде здійснюватися POST запит
curl_setopt($ch, CURLOPT_POST, 1);
// Передаємо тіло POST запиту
curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

/* Вказуємо додаткові дані для заголовка:
     Content-Type - тип вмісту,
     boundary - роздільник і
     Content-Length - довжина тіла повідомлення */
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data; boundary=' . $delimiter,
    'Content-Length: ' . strlen($post)));

// Надсилаємо POST запит на віддалений Web сервер
curl_exec($ch);