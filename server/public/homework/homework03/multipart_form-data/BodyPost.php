<?php
/**
 * BodyPost
 * php version 8.0
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181/filter.php
 */

class BodyPost
{
    /**
     * Метод формування частини складеного запиту
     * @param mixed $name name
     * @param mixed $val  val
     * @return string
     */
    public static function PartPost($name, $val)
    {
        $body = 'Content-Disposition: form-data; name="' . $name . '"';
// Перевіряємо чи передано клас oFile
        if ($val instanceof oFile) {
// Витягуємо ім'я файлу
            $file = $val->Name();
// Витягуємо MIME тип файлу
            $mime = $val->Mime();
// Витягуємо вміст файлу
            $cont = $val->Content();

            $body .= '; filename="' . $file . '"' . "\r\n";
            $body .= 'Content-Type: ' . $mime ."\r\n\r\n";
            $body .= $cont."\r\n";
        } else $body .= "\r\n\r\n".urlencode($val)."\r\n";
        return $body;
    }

    /**
     * Метод, що формує тіло POST запиту з переданого масиву
     * @param array $post
     * @param $delimiter
     * @return string
     * @throws Exception
     */
    public static function Get(array $post, $delimiter = '-------------0123456789')
    {
        if(is_array($post) && !empty($post)) {
            $bool = false;
// Перевіряємо чи є серед елементів масиву файл
            foreach($post as $val) { if($val instanceof oFile) { $bool = true; break; }}
            if ($bool) {
                $ret = '';
                foreach ($post as $name => $val) {
                    $ret .= ' -- ' . $delimiter. "\r\n". self::PartPost($name, $val);
                }
                $ret .= ' -- ' . $delimiter . "--\r\n";
            } else $ret = http_build_query($post);
        } else throw new \Exception('Error input param!');
        return $ret;
    }
};
