<?php

/**
 * Homework_03 Form delivery
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */

session_start();
if ($_REQUEST['submit']) {
    if (!empty($_REQUEST['product'] && $_REQUEST['count'])) {
        $_SESSION['cart'] = array();
    }
    $mas = [
        'product_id' => $_REQUEST['product'],
        'count' => $_REQUEST['count'],
    ];
    array_push($_SESSION['cart'], $mas);
}
echo 'До корзини додані наступні товари <br>';
foreach ($_SESSION['cart'] as $value) {
    echo '<pre>';
    echo 'ID товару: ' . $value['product_id'] . ' в кількості - ' .  $value['count'] . 'шт.' . '<br>';
    echo '</pre>';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <title>hello world because I love stereotypes</title>
</head>
<body>

<form action="<?php $_SERVER['SCRIPT_NAME']?>" style="max-width: 300px" method="post" enctype="multipart/form-data">
    <select class="form-select" name="product" aria-label="Multiple select example">
        <option value="phone">Талефон</option>
        <option value="tv">Телевізор</option>
        <option value="mouse">Миш</option>
        <option value="laptop">Ноутбук</option>
        <option value="computer">Компю'тер</option>
        <option value="pen">Ручка</option>
        <option value="monitor">Монітор</option>
        <option value="keyboard">Клавіатура</option>
        <option value="knife">Ніж</option>
    </select>
    <div class="mb-3">
        <label for="count">Кількість товару</label>
        <input type="number" class="form-control" name="count" id="count" value="submit">
    </div>
    <div class="mb-3">
        <label for="submit"></label>
        <input type="submit" class="btn btn-primary" name="submit" id="submit" value="submit">
    </div>
</form>

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>
</body>
</html>
