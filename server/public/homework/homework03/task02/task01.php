<?php
/**
 * Homework_03 form
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */

session_start();

$config = include 'config.php';

$key = $config['secret_key'];
$tokenLifetime = 3600; // 1 година

/**
 * Test CRF token
 *
 * @param string $key maybe string
 *
 * @return string
 */
function generateToken($key)
{
    $tokenData = [
        'user_ip' => $_SERVER['REMOTE_ADDR'],  // Опціонально: прив'язати токен до IP користувача
        'timestamp' => time(),
    ];

    $token = json_encode($tokenData);
    $iv = openssl_random_pseudo_bytes(16); // Ініціалізаційний вектор
    $encryptedToken = openssl_encrypt($token, 'AES-256-CBC', $key, 0, $iv);
    return base64_encode($iv . $encryptedToken);
}

// Перевірка часу життя токена і його оновлення
if (!isset($_SESSION['form_token']) || (time() - json_decode(openssl_decrypt(substr(base64_decode($_SESSION['form_token']), 16), 'AES-256-CBC', $key, 0, substr(base64_decode($_SESSION['form_token']), 0, 16)), true)['timestamp']) > $tokenLifetime) {
    $_SESSION['form_token'] = generateToken($key);
    // Оновлення cookie з новим токеном
    setcookie(
        'csrf_token', $_SESSION['form_token'],
        ['expires' => time() + $tokenLifetime,
        'path' => '/',
        'secure' => true,
        'httponly' => true,
        'samesite' => 'Strict',]
    );
}
file_put_contents('token_log.txt', 'Згенерований токен: ' . $_SESSION['form_token'] . "\n", FILE_APPEND);
// Обробка форми після надсилання
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit'])) {
    // Перевірка наявності та коректності токена
    if (!empty($_COOKIE['csrf_token']) && isset($_SESSION['form_token'])) {
        $receivedToken = base64_decode($_COOKIE['csrf_token']);
        $iv = substr($receivedToken, 0, 16);
        $encryptedToken = substr($receivedToken, 16);
        $decryptedToken = openssl_decrypt($encryptedToken, 'AES-256-CBC', $key, 0, $iv);
        $tokenData = json_decode($decryptedToken, true);


        // Перевірка дійсності токена (наприклад, що він був згенерований протягом останньої години)
        if ($tokenData && $tokenData['timestamp'] > (time() - $tokenLifetime) && $tokenData['user_ip'] === $_SERVER['REMOTE_ADDR']) {
            // Токен дійсний, продовжуємо обробку форми
            if ($_REQUEST['submit']) {
                if (!empty($_REQUEST['name'] && $_REQUEST['comment'])) {
                    $_REQUEST['comment'] = trim($_REQUEST['comment']);
                    $_REQUEST['comment'] = strip_tags($_REQUEST['comment']);
                    $_REQUEST['comment'] = htmlspecialchars($_REQUEST['comment']);

                    $_REQUEST['name'] = trim($_REQUEST['name']);
                    $_REQUEST['name'] = strip_tags($_REQUEST['name']);
                    $_REQUEST['name'] = htmlspecialchars($_REQUEST['name']);
                    echo '<pre>';
                    echo 'Користувач: ' . $_REQUEST['name'] . '<br><p>Коментар:<br>' . $_REQUEST['comment'] . '</p>';
                    echo '</pre>';
                } else {
                    echo '<pre>';
                    echo 'Заповніть поля';
                    echo '</pre>';
                }
            } else {
                echo '<pre>';
                echo 'Недійсний фбо застарілий токен';
                echo '</pre>';
            }
        } else {
            echo '<pre>';
            echo 'Токен не знайдено або він недійсний';
            echo '</pre>';
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <title>hello world because I love stereotypes</title>
</head>
<body>

<form action="<?php $_SERVER['SCRIPT_NAME']?>" style="max-width: 300px" method="post" enctype="multipart/form-data">
    <div class="mb-3">
        <label for="you_name" class="form-label">Your name</label>
        <input type="text" class="form-control mb-3 w-100" name="name" id="you_name" placeholder="Іван Багряний">
    </div>
    <div class="mb-3">
        <label for="comment" class="form-label">Your comment</label>
        <textarea class="form-control" name="comment" id="comment" rows="3" placeholder="Напишіть свій коментар"></textarea>
    </div>
    <input type="hidden" name="token" value="<?php echo htmlspecialchars($_SESSION['form_token']);?>">
    <div class="mb-3">
        <label for="submit"></label>
        <input type="submit" class="btn btn-primary" name="submit" id="submit" value="submit">
        <label for="javascript"></label>
        <input type="reset" id="javascript" class="btn btn-danger" name="reset" value="reset" onclick="comment();">
    </div>
</form>

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>
<script> function comment() {
        document.getElementById('clear').value = '';
    }
</script>
</body>
</html>
