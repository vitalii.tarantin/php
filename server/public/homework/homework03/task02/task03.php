<?php

/**
 * Homework_03 Form delivery
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */

session_start();

if ($_REQUEST['submit']) {
    if ($_REQUEST['name'] && $_REQUEST['phone']) {
        $name = $_REQUEST['name'];
        $name = trim($name);
        $name = strip_tags($name);
        $name = htmlspecialchars($name);
        $phone = $_REQUEST['phone'];
        $phone = trim($phone);
        $phone = strip_tags($phone);
        $phone = htmlspecialchars($phone);
        echo '<pre>Ми обовязково ' .  $name . ' вам перетелефонуємо</pre>';
        echo '<pre>Ваше Імя: ' . $name . '<br>Ваш номер телефому - </pre>' . $phone;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <title>hello world because I love stereotypes</title>
</head>
<body>

<form action="<?php $_SERVER['SCRIPT_NAME']?>" style="max-width: 300px" method="post" enctype="multipart/form-data">
    <div class="mb-3">
        <label for="you_name" class="form-label">Ваше ім'я</label>
        <input type="text" class="form-control" name="name" id="you_name" placeholder="Іван Багряний">
    </div>
    <div class="mb-3">
        <label for="phone">Введіть ваш номер телевону: </label>
        <input type="tel"
               class="form-control"
               name="phone"
               id="phone"
               pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
               placeholder="063-111-11-11"
        >
    </div>
    <div class="mb-3">
        <label for="submit"></label>
        <input type="submit" class="btn btn-primary" name="submit" id="submit" value="submit">
    </div>
</form>

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>
</body>
</html>

