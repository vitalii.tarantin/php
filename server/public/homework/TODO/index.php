<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>hello world</title>
    <link rel="stylesheet" href="styles/main.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <h2>TO DO list</h2>
    <form action="add.php" method="post">
        <input aria-label="to do" type="text" name="task" id="task" class="form-control" placeholder="To Do...">
        <button type="submit" name="sendTask" class="btn btn-success">Send</button>
    </form>
    <?php
        require 'ConfigDB.php';

        $query = $pdo->query('SELECT * FROM `tasks` ORDER BY `id` DESC');

        echo '<ul>';
        while($row = $query->fetch(PDO::FETCH_OBJ)) {
            echo '<li><b>'.$row->task.'</b><button><a style="text-decoration: none; color: black;" href="delete.php?id='.$row->id.'">Delete</button></li>';
        }
        echo '</ul>';
        ?>
</div>
<script src="js/main.js"></script>
</body>
</html>