<?php

/**
 * @param $value
 * @param $filename
 * @return void
 */
function objectTwoFiles($value, $filename): void
{
    $str_value = serialize($value);
    $f = fopen($filename, "w");
    fwrite($f, $str_value);
    fclose($f);
}
objectTwoFiles();
/**
 * @param $filename
 * @return mixed
 */
function object_from_file($filename): mixed
{
    $file = file_get_contents($filename);
    $value = unserialize($file);
    return $value;
}

$arrayInsertionSort = (object_from_file('array.txt'));

$start = microtime(true);
$b = bubbleSort($arrayInsertionSort);
$time = microtime(true) - $start;
echo 'bubbleSort => ' . $time . "\n";

$start = microtime(true);
$c = insertionSort($arrayInsertionSort);
$time = microtime(true) - $start;
echo 'insertionSort => ' . $time . "\n";

$start = microtime(true);
$d = shellSort($arrayInsertionSort);
$time = microtime(true) - $start;
echo 'shellSort => ' . $time . "\n";

var_dump($b == $c, $b == $d, $c == $d);
/**
 * @param $arr
 * @return mixed
 */
function insertionSort($arr): mixed
{
    $arrCount = count($arr);
    for ($i = 0; $i <= $arrCount - 2; $i++) {
        $k = $i;
        for ($j =$i + 1; $j > 0; $j--) {
            if ($arr[$j] < $arr[$k]) {
                $temp = $arr[$j];
                $arr[$j] = $arr[$k];
                $arr[$k] = $temp;
            }
            if ($k > 0) {
                $k--;
            }
        }
    }
    return $arr;
}
 echo insertionSort([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
/**
 * @param $arr
 * @return mixed
 */
function bubbleSort($arr): mixed
{
    $count = count($arr) - 1;
    for ($i = $count; $i >= 0; $i--) {
        for ($j = 0; $j <= ($i - 1); $j++) {
            if ($arr[$j] > $arr[$j + 1]) {
                $k = $arr[$j];
                $arr[$j] = $arr[$j + 1];
                $arr[$j + 1] = $k;
            }
        }
    }
    return $arr;
}
echo bubbleSort([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
/**
 * @param $array
 * @return mixed
 */
 function shellSort($array): mixed {
     $length = count($array) - 1;
     $k = 0;
     $gap[0] = (int)($length / 2);
     while ($gap[$k] > 1 ) {
         $k++;
         $gap[$k] = (int)($gap[$k - 1] / 2);
     }
     for ($i =0; $i <= $k; $i++ ) {
         $step = $gap[$i];
         for ($j = $step; $j < $length; $j++) {
             $temp = $array[$j];
             $p = $j - $step;
             while ($p >= 0 && $temp < $array[$p] ) {
                 $array[$p + $step] = $array[$p];
                 $p = $p - $step;
             }
             $array[$p+$step] = $step;
         }
     }
     return $array;
 }
 echo shellSort([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);