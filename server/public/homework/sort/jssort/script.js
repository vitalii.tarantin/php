'use strict';

/**
 * @param arr
 * @returns {*}
 */
function insertionSort(arr) {
    let arrCount = arr.length;
    for (let i = 0; i <= arrCount - 2; i++) {
        let k = i
        for (let j = i + 1; j > 0; j--) {
            if (arr[j] < arr[k]) {
                let temp = arr[j]
                arr[j] = arr[k]
                arr[k] = temp
            }
            if (k > 0)
                k--
        }
    }
    return arr
}
let arr = [3, 35, 21, 78, 7, 55, 13, 42, 8, 89, 1, 87, 36, 99, 4];
console.log(insertionSort(arr));

//----------------------------------------------------------------------------------
/**
 *
 * @param array
 * @returns {*}
 */
function shellSort(array) {
    let length = array.length
    let k = 0
    let gap = []
    gap[0] = Math.floor(length  / 2)
    while (gap[k] > 1 )
    {
        k++
        gap[k] = Math.floor(gap[k-1] / 2)
    }
    for (let i = 0; i <= k; i++)
    {
        let step = gap[i]
        for (let j = step; j < length; j++)
        {
            let temp = array[j]
            let p = j - step
            while (p>=0 && temp < array[p])
            {
                array[p + step] = array[p]
                p = p - step
            }
            array[p+step] = temp
        }
    }
    return array
}
let array = [3, 35, 21, 78, 7, 55, 13, 42, 8, 89, 1, 87, 36, 99, 4];
console.log(shellSort(array))
//--------------------------------------------------------------------------------
/**
 *
 * @returns {number[]}
 */
function bubbleSort (array)
{
    let count = arr.length - 1
    for (let i = count; i>=0; i--)
    {
        for (let j = 0; j <= (i-1); j++)
        {
            if (arr[j] > arr[j + 1])
            {
                let k = arr[j]
                arr[j] = arr[j+1]
                arr[j +1] = k
            }
        }
    }
    return arr
}
let arr = [3, 35, 21, 78, 7, 55, 13, 42, 8, 89, 1, 87, 36, 99, 4];
console.log(bubbleSort(arr))