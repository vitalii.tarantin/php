<?php

/**
 * Homework_05 unit-03
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */

require 'helpers.php';

/**
 * FindPythagorasTheorem
 *
 * @param float $sideOfFirstTriangle  param
 * @param float $sideOfSecondTriangle param
 *
 * @return float
 */
$findPythagorasTheorem = function (float $sideOfFirstTriangle, float $sideOfSecondTriangle): float {
    return sqrt(pow($sideOfFirstTriangle, 2) + pow($sideOfSecondTriangle, 2));
};

/**
 * FindPerimeter
 *
 * @param float $ideOne  param
 * @param float $sideTwo param
 *
 * @return float
 */
$findPerimeter = function (float $ideOne, float $sideTwo): float {
    return 2 * ($sideTwo + $ideOne);
};

/**
 * CalcDiscriminant
 *
 * @param integer $a param
 * @param integer $b param
 * @param integer $c param
 *
 * @return float|int|object
 */

$calcDiscriminant = function (int $a, int $b, int $c) {
    return pow($b, 2) - 4 * $a * $c;
};
print 'Your Discriminant is: ' . $calcDiscriminant(3, 3, 3);

/**
 * GetEvenNumbers
 *
 * @param int $number param
 *
 * @return array
 */
$getEvenNumbers = function (int $number): array {
    $evenNumber = [];
    for ($i = 1; $i <= $number; $i++) {
        if ($i % 2 == 0) {
            $evenNumber[] = $i;
        }
    }
    return $evenNumber;
};

/**
 * GetOddNumbers
 *
 * @param int $number param
 *
 * @return array
 */
$getOddNumbers = function (int $number): array {
    $oddNumber = [];
    for ($i = 1; $i <= $number; $i++) {
        if ($i % 2 != 0) {
            $oddNumber[] = $i;
        }
    }
    return $oddNumber;
};

/**
 * CheckArray
 *
 * @param array $arrayCheck param
 *
 * @return bool
 */
$checkArray = function (array $arrayCheck): bool {
    foreach (array_count_values($arrayCheck) as $value) {
        if ($value > 1) {
            return false;
        }
    }
    return true;
};
for ($i = 1; $i < 9; $i++) {
    $arrayCheck[] = rand(1, 9);
}
if ($checkArray($arrayCheck)) {
    echo '<br> Elements are not repeated <br>';
} else {
    echo '<br>Elements are repeated<br>';
}
/**
 * GetSortBubble
 *
 * @param array $array param
 * @param string $sort param
 *
 * @return array
 */
$getSortBubble = function (array $array, string $sort = 'desc'): array {
    $count = count($array);
    if ($sort == 'asc') {
        for ($i = 0; $i < $count - 1; $i++) {
            for ($j = 0; $j < $count - $i - 1; $j++) {
                if ($array[$j] < $array[$j + 1]) {
                    $temp = $array[$j];
                    $array[$j] = $array[$j + 1];
                    $array[$j + 1] = $temp;
                }
            }
        }
    } else {
        for ($i = 0; $i < $count - 1; $i++) {
            for ($j = 0; $j < $count - 1 - $i; $j++) {
                if ($array[$j] > $array[$j + 1]) {
                    $temp = $array[$j];
                    $array[$j] = $array[$j + 1];
                    $array[$j + 1] = $temp;
                }
            }
        }
    }
    return $array;
};
$array = [4,8,1,7,3,0,2,9,6,5];
$getSortBubble($array, 'asc');
