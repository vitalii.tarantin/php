<?php

/**
 * Homework_06 arrow_function
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */

/**
 * CalcSquareFigure
 *
 * @param float $topBase    param
 * @param float $bottomBase param
 * @param float $height     param
 *
 * @return float
 */
$calcSquareFigure = fn(float $topBase, float $bottomBase, float $height)
=> 0.5 * ($topBase + $bottomBase) * $height;
print $calcSquareFigure(3.5, 4.3, 7.4) . 'sm<sup>2</sup>';
/**
 * FindPythagorasTheorem
 *
 * @param float $FirstTriangle  param
 * @param float $SecondTriangle param
 *
 * @return float
 */
$findPythagorasTheorem = fn (float $FirstTriangle, float $SecondTriangle): float
=>  sqrt(pow($FirstTriangle, 2) - pow($SecondTriangle, 2));
echo $findPythagorasTheorem(5, 3);
/**
 * FindPerimeter
 *
 * @param float $ideOne  param
 * @param float $sideTwo param
 *
 * @return float
 */
$findPerimeter = fn (float $ideOne, float $sideTwo): float
=> 2 * ($sideTwo + $ideOne);

print 'Your perimeter triangle ' .  $findPerimeter(4, 4) . ' sm<sup>2</sup>';
/**
 * CalcDiscriminant
 *
 * @param integer $a param
 * @param integer $b param
 * @param integer $c param
 *
 * @return float|int|object
 */
$calcDiscriminant = fn(int $a, int $b, int $c) => pow($b, 2) - 4 * $a * $c;
print 'Your Discriminant is: ' . $calcDiscriminant(3, 3, 3);
