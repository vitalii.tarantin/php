<?php

/**
 * Homework_04 loop while
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */

/**
 * Function dd
 *
 * @param array $ded param array
 *
 * @return array
 */
function dd(array $ded): array
{
    echo '<pre>';
    print_r($ded);
    echo '</pre>';
    return $ded;
}
