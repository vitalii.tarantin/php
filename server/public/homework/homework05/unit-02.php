<?php

/**
 * Homework_05 unit-03
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */

require 'helpers.php';

/**
 * 1. Create a function to find a number in degree
 *
 * @param array|integer $number param
 * @param array|integer $pow    param
 *
 * @return integer
 */
function calcPowNumber(array|int $number, array|int $pow): int
{
    return $number ** $pow;
}
print calcPowNumber(3, 3);
/**
 * 1. Create a function anonymus to find a number in degree
 *
 * @param int $number param
 * @param int $pow    param
 *
 * @return mixed
 */
$calcPowNumber = function (int $number, int $pow): int {
    return $number ** $pow;
};
print $calcPowNumber(3, 4);
/**
 * 1. Create a function arrow to find a number in degree
 *
 * @param int $number param
 * @param int $pow    param
 *
 * @return mixed
 */
$calcPowNumberArrow = fn (int $number, int $pow): int => $number ** $pow;
print $calcPowNumberArrow(3, 4);
/**
 * 2. Write a sorting function. The function accepts an array of random numbers and sorts them in  order. By default, the function sorts in ascending order. But if you pass in a parameter, the  function will sort in descending order.
  sort(arr)
  sort(arr, 'asc')
  sort(arr, 'desc')
 *
 * @param array        $array param
 * @param array|string $sort  param
 *
 * @return array
 */
function getSort(array $array, array|string $sort = 'desc'): array
{
    $count = count($array);
    if ($sort == 'asc') {
        for ($i = 0; $i < $count - 1; $i++) {
            for ($j = 0; $j < $count - $i - 1; $j++) {
                if ($array[$j] < $array[$j + 1]) {
                    $temp = $array[$j];
                    $array[$j] = $array[$j + 1];
                    $array[$j + 1] = $temp;
                }
            }
        }
    } else {
        for ($i = 0; $i < $count - 1; $i++) {
            for ($j = 0; $j < $count - 1 - $i; $j++) {
                if ($array[$j] > $array[$j + 1]) {
                    $temp = $array[$j];
                    $array[$j] = $array[$j + 1];
                    $array[$j + 1] = $temp;
                }
            }
        }
    }
    return $array;
}
$array = [4,8,1,7,3,0,2,9,6,5];
dd(getSort($array, 'asc'));
/**
 * 3. Write an array search function. the function will take two parameters. The first is an array, the second is a search number. search(arr, find)
 *
 * @param array         $array       param
 * @param array|integer $valueNumber param
 *
 * @return array
 */
function findKeyInArray(array $array, array|int $valueNumber): array
{
    $notFindNumber = [];
    foreach ($array as $key => $value) {
        if ($value == $valueNumber) {
            $result[$key] = $value;
            return $result;
        }
    }
    return $notFindNumber;
}
$array = [42, 13, 37, 84, 66, 10, 59, 21, 89, 78];
findKeyInArray($array, 21);
