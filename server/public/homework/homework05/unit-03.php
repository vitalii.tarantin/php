<?php

/**
 * Homework_05 unit-03
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */

require 'helpers.php';
/**
 * Write your own print_r() function. Use recursion to solve this problem.
 *
 * @param array $variable param
 *
 * @return string
 */
function myPrintR(array $variable)
{
    if (is_array($variable)) {
        echo "Array\n(\n";
        foreach ($variable as $key => $value) {
            if (is_array($value)) {
                echo sprintf('    [%s] => ', $key);
                myPrintR($value);
            } else {
                echo sprintf('    [%s] => %d\n', $key, $value);
            }
        }
        echo ")\n";
    } elseif (is_string($variable)) {
        echo $variable . "\n";
    } elseif (is_numeric($variable)) {
        echo $variable . "\n";
    } elseif (is_bool($variable)) {
        echo $variable ? 'true' : 'false';
        echo "\n";
    } elseif (is_null($variable)) {
        echo "NULL\n";
    }
    return $variable;
}
$array = ['My', 'function', 'print_r', ['works', 'not', ['I', 'Love', 'Rocks', 'Music'] ], 'bad' ];
