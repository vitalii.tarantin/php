<?php

/**
 * Homework_05 unit-01
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */

session_start();

require 'form.html';

require 'helpers.php';


$numOne = rand(1, 100);
$numTwo = rand(1, 100);
$numThree = rand(1, 100);

$array = [];
for ($i = 0; $i < 100; $i++) {
    $array[$i] = rand(0, 100);
}
/**
 * FUnction checkEvenAndAboveZeroNumbers You need to create an array and fill it with random numbers from 1 to 100 (rand function). Then, calculate the product of those elements that are greater than zero and have even indices. After that, display the elements that are greater than zero and have odd indexes.
 *
 * @param array $array param array
 *
 * @return mixed
 */
function checkEvenAndAboveZeroNumbers(array $array): mixed
{
    $evenAboveZeroNumbers = 1;
    for ($i = 0; $i < 10; $i++) {
        if ($array[$i] > 0 && $i % 2 == 0) {
            $evenAboveZeroNumbers *= $array[$i];
        }
    }
    return $evenAboveZeroNumbers;
}
 echo checkEvenAndAboveZeroNumbers($array);
/**
 * FUnction checkOddAndAboveZeroNumbers
 *
 * @param array $array param array
 *
 * @return array
 */
function checkOddAndAboveZeroNumbers(array $array): mixed
{
    $oddAndAboveZeroNumbers = 1;
    for ($i = 0; $i < 10; $i++) {
        if ($i % 2 !== 0) {
            $oddAndAboveZeroNumbers *= $array[$i];
        }
    }
    return $oddAndAboveZeroNumbers;
}
echo checkOddAndAboveZeroNumbers($array);

/**
 * 2. Two numbers are given. Find their sum and product. Two numbers are given. Find the sum of their squares.
 *
 * @param array|integer $numOne param int
 * @param array|integer $numTwo param int
 *
 * @return integer
 */
function getSumNum(array|int $numOne, array|int $numTwo): int
{
    return $numOne + $numTwo;
}
echo getSumNum($numOne, $numTwo) . '<br>';

/**
 * Function getSumSquares
 *
 * @param array|integer $numOne int
 * @param array|integer $numTwo int
 *
 * @return integer
 */
function getSumSquares(array|int $numOne, array|int $numTwo): int
{
    return $numOne ** 2 + $numTwo ** 2;
}
echo getSumSquares($numOne, $numTwo);


/**
 * 3. Three numbers are given. Find their arithmetic mean.
 *
 * @param array|float ...$numbers float
 *
 * @return float
 */
function getAverageNumbers(array|float ...$numbers): float
{
    return array_sum($numbers) / count($numbers);
}
echo getAverageNumbers(63, 63, 19);
/**
 * Function getIncreasedByPercent 4. A number is given. Increase it by 30%, by 120%.
 *
 * @param array|integer $numbers param int
 * @param array|integer $percent param int
 *
 * @return float
 */
function getIncreasedByPercent(array|int $numbers, array|int $percent): float
{
    return $numbers + ($numbers * $percent / 100);
}
echo getIncreasedByPercent(100, 120);
/**
 * 5. The user selects a country (Turkey, Egypt or Italy) from the drop-down list, enters the number of days for the vacation and indicates if there is a discount (checkbox). Output the cost of the vacation, which is calculated as the product of the number of days by 400. This number is then increased by 10% if Egypt is selected and by 12% if Italy is selected. And then this number decreases by 5% if a discount is specified.
 *
 * @param array|float   $discount param
 * @param array|string  $country  param
 * @param array|integer $days     param
 *
 * @return float
 */
function calcVacationDays(array|float $discount, array|string $country, array|int $days): float
{
    return $days * 400 + $days * 400 * $country - $days * 400 * $country * $discount;
}
if ($_REQUEST['submitVacation']) {
    $days = htmlspecialchars($_REQUEST['days']);
    $discount = $_REQUEST['discount'];
    $country = $_REQUEST['country'];
    if ($discount == 'on') {
        $discount = 0.05;
    } else {
        $discount = 0;
    } if ($country == 'Egypt') {
        $country = 0.1;
    } elseif ($country == 'Italy') {
        $country = 0.12;
    } else {
        $country = 0;
    }
}
echo 'Amount vacation ' . calcVacationDays($discount, $country, $days) . 'EUR';
/**
 * 6. User enters his name, password, email. If all information is specified, then show these data after the phrase Registration was successful, otherwise report which of the fields was
  not filled in.
 *
 * @param array|string $name     param
 * @param array|string $email    param
 * @param array|string $password param
 *
 * @return string
 */
function checkRegistrationForm(array|string $name, array|string $email, array|string $password): string
{
    $email = htmlspecialchars($_REQUEST['registrationEmail']);
    $name = htmlspecialchars($_REQUEST['registrationName']);
    $password = htmlspecialchars($_REQUEST['password']);
    if (!empty($name && $email && $password)) {
        echo 'Registration successful';
    } elseif (empty($name)) {
        echo 'enter your name';
    } elseif (empty($email)) {
        echo 'enter your email';
    } elseif (empty($password)) {
        echo 'enter your password';
    }
    return true;
}
checkRegistrationForm($_REQUEST['registrationName'], $_REQUEST['registrationEmail'], $_REQUEST['password']);
/**
 * 7. Display the phrase “Silence is golden” n times. The number n is entered by the user on the form. If n is incorrect, output the phrase “Bad n”.
 *
 * @param array|integer $numberRepeat param
 *
 * @return string
 */
function getTextRepeat(array|int $numberRepeat): string
{
    for ($i = 0; $i < $numberRepeat; $i++) {
        echo 'Silence is golden' . '<br>';
    }
    return true;
}
if (!empty($_REQUEST['submitSilenceIsGolden'])) {
    $numberRepeat = htmlspecialchars($_REQUEST['number']);
    $numberRepeat = strip_tags($numberRepeat);
    $numberRepeat = htmlspecialchars($numberRepeat);
    if (is_numeric($numberRepeat)) {
        getTextRepeat($numberRepeat);
    } else {
        echo 'Bad n you enter the string please try again. enter yor number in integer';
    }
}

/**
 * 8. Fill an array of length n with zeros and ones, with the given values alternating starting from zero.
 *
 * @param array|integer $numbersZeroAndOne param
 *
 * @return array
 */
function fillArrayWithZeroAndOne(array|int $numbersZeroAndOne): array
{
    $array = [];
    for ($i = 0; $i < $numbersZeroAndOne; $i++) {
        $array[] = $i % 2;
    }
    return $array;
}
if (!empty($_REQUEST['submitSilenceIsGolden'])) {
    $numbersZeroAndOne = htmlspecialchars($_REQUEST['number']);
}
$arrayCheck = [1,2,3,4,5,1,2,3,4,5];
/**
 * 9. Determine if there are repeating elements in the array
 *
 * @param array $arrayCheck param
 *
 * @return boolean
 */
function checkArray(array $arrayCheck): bool
{
    foreach (array_count_values($arrayCheck) as $value) {
        if ($value > 1) {
            return false;
        }
    }
    return true;
}

if (checkArray($arrayCheck)) {
    echo '<pre>';
    echo '<br>Elements are not repeated<br>';
    echo '</pre>';
} else {
    echo '<pre>';
    echo '<br>Elements are repeated<br>';
    echo '</pre>';
}
print '<pre>10. Find the minimum and maximum among 3 numbers</pre>';
/**
 * Find max among 3 numbers
 *
 * @param array|integer $numberFirst  param
 * @param array|integer $numberSecond param
 * @param array|integer $numberThird  param
 *
 * @return boolean
 */
function declarationMax(array|int $numberFirst, array|int $numberSecond, array|int $numberThird): bool
{
    if ($numberFirst > $numberSecond && $numberThird < $numberFirst) {
        print sprintf('numberFirst=%s is max  numberSecond=%s and numberThird=%s', $numberFirst, $numberSecond, $numberThird);
    } elseif ($numberFirst < $numberSecond && $numberThird < $numberSecond) {
        print sprintf('numberSecond=%s is max numberThird=%s and numberFirst=%s', $numberSecond, $numberThird, $numberFirst);
    } elseif ($numberFirst < $numberThird && $numberThird > $numberSecond) {
        print sprintf('numberThird=%s is max  numberFirst=%s and numberSecond=%s', $numberThird, $numberFirst, $numberSecond);
    }
    return true;
}
declarationMax(6, 8, 10);
echo '<br>';
/**
 * Find a min among 3 numbers
 *
 * @param array|integer $numberFirst  param
 * @param array|integer $numberSecond param
 * @param array|integer $numberThird  param
 *
 * @return boolean
 */
function declarationMin(array|int $numberFirst, array|int $numberSecond, array|int $numberThird): bool
{
    if ($numberFirst < $numberSecond && $numberThird > $numberFirst) {
        print sprintf('numberFirst=%s is min  numberSecond=%s and numberThird=%s', $numberFirst, $numberSecond, $numberThird);
    } elseif ($numberFirst > $numberSecond && $numberThird > $numberSecond) {
        print sprintf('numberSecond=%s is min numberThird=%s and numberFirst=%s', $numberSecond, $numberThird, $numberFirst);
    } elseif ($numberFirst > $numberThird && $numberThird < $numberSecond) {
        print sprintf('numberThird=%s is min  numberFirst=%s and numberSecond=%s', $numberThird, $numberFirst, $numberSecond);
    }
    return true;
}
declarationMin(6, 8, 10);

/**
 * 11. Find the area of
 *
 * @param array|float $a param
 * @param array|float $b param
 *
 * @return float
 */
function getSquareFigure(array|float $a, array|float $b): float
{
    return 0.5 * $a * $b;
}
print 'Your square triangle ' .  getSquareFigure(3, 3) . 'sm<sup>2</sup>';

/**
 * 12. Pythagoras theorem
 *
 * @param array|float $sideOfFirstTriangle  param
 * @param array|float $sideOfSecondTriangle param
 *
 * @return float
 */
function findPythagorasTheorem(array|float $sideOfFirstTriangle, array|float $sideOfSecondTriangle): float
{
    return sqrt(pow($sideOfFirstTriangle, 2) + pow($sideOfSecondTriangle, 2));
}
print findPythagorasTheorem(4.28, 4.21) . 'sm<sup>2</sup>';

/**
 * 13. To find the perimeter
 *
 * @param array|float $a param
 * @param array|float $b param
 *
 * @return float
 */
function calcPerimeter(array|float $a, array|float $b): float
{
    return 2 * ($a + $b);
}
print 'Your perimeter triangle ' .  calcPerimeter(3, 3) . 'sm<sup>2</sup>';

/**
 * 14. Find the discriminant<
 *
 * @param array|float $a param
 * @param array|float $b param
 * @param array|float $c param
 *
 * @return float
 */
function calcDiscriminant(array|float $a, array|float $b, array|float $c): float
{
    return pow($b, 2) - 4 * $a * $c;
}
print 'Your Discriminant is: ' . calcDiscriminant(3, 3, 3);
/**
 * 15. Create only even numbers up to 100
 *
 * @param array|integer $evenNumbers param
 *
 * @return array
 */
function getEvenNumbers(array|int $evenNumbers): array
{
    $arrayEven = [];
    for ($i = 1; $i <= $evenNumbers; $i++) {
        if ($i % 2 == 0) {
            $arrayEven[] = $i;
        }
    }
    return $arrayEven;
}
dd(getEvenNumbers(100));

/**
 * 16. Create only odd numbers up to 100
 *
 * @param array|integer $evenNumbers param
 *
 * @return array
 */
function getOddNumbers(array|int $evenNumbers): array
{
    $arrayEven = [];
    for ($i = 1; $i <= $evenNumbers; $i++) {
        if ($i % 2 !== 0) {
            $arrayEven[] = $i;
        }
    }
    return $arrayEven;
}
echo getOddNumbers(100);
