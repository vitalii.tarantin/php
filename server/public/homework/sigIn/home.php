<?php

/**
 * Test register and sigIn
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */

require_once __DIR__ . '/src/helpers.php';

$user = currentUser();
?>
<!DOCTYPE html>
<html lang="en" data-theme="dark">
<?php require_once __DIR__ . '/components/head.php' ?>
<body>
<div class="card home">
    <a href="https://ibb.co/X41zJP1">
        <img
                class="avatar"
                src="<?php echo $user['avatar']?>"
                alt="<?php echo $user['avatar']?>"
                border="0">
    </a>
    <h1>Hello World(
        <span style="text-transform: capitalize">
            <?php echo $user['name']?>
        </span>
        )! because I love stereotypes</h1>
    <form action="src/action/logout.php" method="post">
        <button role="button">Log out of your account!</button>
    </form>
</div>
<?php require_once __DIR__ . '/components/script.php' ?>
</body>
</html>
