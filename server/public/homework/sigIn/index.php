<?php

/**
 * Test register and sigIn
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */

require_once __DIR__ . '/src/helpers.php';

?>

<!DOCTYPE html>
<html lang="en" data-theme="dark">
<?php require_once __DIR__ . '/components/head.php' ?>
<body>
<form action="src/action/login.php" class="card" method="post" enctype="multipart/form-data">
    <h2>Log In</h2>
    <?php if (hasMessage('error')) : ?>
    <div class="notice error"><?php echo getMessage('error') ?></div>
    <?php endif; ?>
    <label for="email">
        Email
        <input
                type="email"
                name="email"
                id="email"
                placeholder="example@mail.su"
                aria-invalid="true"
                value="<?php echo old('email') ?>"
            <?php validationErrorAttr('email'); ?>
        >
        <?php if (hasValidationError('email')) : ?>
            <small><?php validationErrorMessage('email');?></small>
        <?php endif; ?>
    </label>
    <label for="password">
        Password
        <input
                type="password"
               id="password"
               name="password"
               placeholder="********"
        >
    </label>
    <button
    type="submit"
    id="submit"
    >Continue</button>
</form>
<p>I don`t have <a href="../../lesson/lesson04/index.php">an account</a> yet</p>
<?php require_once __DIR__ . '/components/script.php' ?>
</body>
</html>
