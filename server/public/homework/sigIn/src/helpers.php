<?php

/**
 * Test register and sigIn
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */

session_start();

require_once __DIR__ . '/config.php';

/**
 * Redirect func
 *
 * @param string $path param
 *
 * @return void
 */
function redirect(string $path)
{
    header("Location: $path");
    die();
}

/**
 * SetValidationError
 *
 * @param string $fieldName param
 * @param string $message   param
 *
 * @return void
 */
function setValidationError(string $fieldName, string $message): void
{
    $_SESSION['validation'][$fieldName] = $message;
}

/**
 * HasValidationError
 *
 * @param string $fieldName param
 *
 * @return boolean
 */
function hasValidationError(string $fieldName): bool
{
    return isset($_SESSION['validation'][$fieldName]);
}

/**
 * ValidationErrorAttr
 *
 * @param string $fieldName param
 *
 * @return string
 */
function validationErrorAttr(string $fieldName): string
{
    return isset($_SESSION['validation'][$fieldName]) ? 'aria-invalid="true"' : '';
}

/**
 * ValidationErrorMessage
 *
 * @param string $fieldName param
 *
 * @return string
 */
function validationErrorMessage(string $fieldName): string
{
    $message = $_SESSION['validation'][$fieldName] ?? '';
    unset($_SESSION['validation'][$fieldName]);
    return $message;
}

/**
 * SetOldValue
 *
 * @param string $key   param
 * @param mixed  $value param
 *
 * @return void
 */
function setOldValue(string $key, mixed $value): void
{
    $_SESSION['old'][$key] = $value;
}

/**
 * OLD
 *
 * @param string $key param
 *
 * @return mixed|string
 */
function old(string $key)
{
    $value = $_SESSION['old'][$key] ?? '';
    unset($_SESSION['old'][$key]);
    return $value;
}

/**
 * UploadFile
 *
 * @param array  $file   param
 * @param string $prefix param
 *
 * @return string
 */
function uploadFile(array $file, string $prefix = ''): string
{
    $uploadPath = __DIR__ . '/../uploads';

    if (!is_dir($uploadPath)) {
        mkdir($uploadPath, 0777, true);
    }

    $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
    $fileName = $prefix . '_' . time() . ".$ext";

    if (!move_uploaded_file($file['tmp_name'], "$uploadPath/$fileName")) {
        die('Server upload error');
    }

    return "uploads/$fileName";
}

/**
 * SetMessage
 *
 * @param string $key     param
 * @param string $message param
 *
 * @return void
 */
function setMessage(string $key, string $message): void
{
    $_SESSION['message'][$key] = $message;
}

/**
 * HasMessage
 *
 * @param string $key param
 *
 * @return boolean
 */
function hasMessage(string $key): bool
{
    return isset($_SESSION['message'][$key]);
}

/**
 * GetMessage
 *
 * @param string $key param
 *
 * @return string
 */
function getMessage(string $key): string
{
    $message = $_SESSION['message'][$key] ?? '';
    unset($_SESSION['message'][$key]);
    return $message;
}

/**
 * GetPDO
 *
 * @return PDO
 */
function getPDO(): PDO
{
    try {
        return new \PDO('mysql:host=' . DB_HOST . ';port=' . DB_PORT . ';charset=utf8;dbname=' . DB_NAME, DB_USERNAME, DB_PASSWORD);
    } catch (\PDOException $e) {
        die("Connection error: {$e->getMessage()}");
    }
}

/**
 * FIndUser
 *
 * @param array|string $email param
 *
 * @return array|boolean
 */
function findUser(array|string $email): array|bool
{
    $pdo = getPDO();

    $stmt = $pdo->prepare('SELECT * FROM users WHERE `email` = :email');
    $stmt->execute(['email' => $email]);
    return $stmt->fetch(\PDO::FETCH_ASSOC);
}

/**
 * CurrentUser
 *
 * @return array|false
 */
function currentUser(): array|false
{
    $pdo = getPDO();

    if (!isset($_SESSION['user'])) {
        return false;
    }
    $userId = $_SESSION['user']['id'] ?? null;

    $stmt = $pdo->prepare('SELECT * FROM users WHERE id = :id');
    $stmt->execute(['id' => $userId]);
    return $stmt->fetch(\PDO::FETCH_ASSOC);
}

/**
 * LogOut
 *
 * @return void
 */
function logout()
{
    unset($_SESSION['user']['id']);
    redirect('/sigIn/index.php');
}

/**
 * CheckAuth
 *
 * @return void
 */
function checkAuth(): void
{
    if (isset($_SESSION['user']['id'])) {
        redirect('/sigIn/index.php');
    }
}

/**
 * CheckGuest
 *
 * @return void
 */
function checkGuest(): void
{
    if (isset($_SESSION['user']['id'])) {
        redirect('/sigIn/home.php');
    }
}
