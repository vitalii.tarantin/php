<?php

require_once __DIR__ . '/../helpers.php';

$email = htmlspecialchars($_POST['email']) ?? null;
$password = htmlspecialchars($_POST['password']) ?? null;

if (empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
    setOldValue('email', $email);
    setValidationError('email', 'Invalid e-mail format');
    setMessage('error', 'Error validation');
    redirect('/sigIn/index.php');
}
$user = findUser($email);
if (!$user) {
    setMessage('error', "User $email does`t find");
    redirect('/sigIn/index.php');
}
 if (!password_verify($password, $user['password'])) {
     setMessage('error', 'Wrong password');
     redirect('/sigIn/index.php');
 }

 $_SESSION['user']['id'] = $user['id'];
 redirect('/sigIn/home.php');