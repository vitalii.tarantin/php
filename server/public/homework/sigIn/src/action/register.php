<?php

require_once __DIR__ . '/../helpers.php';


$avatarPath = null;
$name= htmlspecialchars($_POST['name']) ?? null;
$email = htmlspecialchars($_POST['email'])  ?? null;
$password = htmlspecialchars($_POST['password'] ) ?? null;
$passwordConfirmation = htmlspecialchars($_POST['password_confirmation'])  ?? null;
$avatar = $_FILES['avatar'] ?? null;


setOldValue('name', $name);
setOldValue('email', $email);


if (empty($name)) {
    setValidationError('name', 'Incorrect name');
}

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    setValidationError('email', 'Incorrect mail');
}

if (empty($password)) {
    setValidationError('password', 'The password is blank');
}

if ($password !== $passwordConfirmation) {
    setValidationError('password', 'Passwords don`t match');
}

if (!empty($avatar)) {
    $types = ['image/jpeg', 'image/png'];

    if (!in_array($avatar['type'], $types)) {
        setValidationError('avatar', 'The profile image is of an incorrect type');
    }

    if (($avatar['size'] / 1000000) >= 1) {
        setValidationError('avatar', 'The image must be less than 1 mb');
    }
}

// If the list with validation errors is not empty, then redirect back to the form.

if (!empty($_SESSION['validation'])) {
    setOldValue('name', $name);
    setOldValue('email', $email);
    redirect('/register.php');
}

//  Upload an avatar if it was sent in the form

if (!empty($avatar)) {
    $avatarPath = uploadFile($avatar, 'avatar');
}

$pdo = getPDO();

$query = "INSERT INTO users (name, email, avatar, password) VALUES (:name, :email, :avatar, :password)";

$params = [
    'name' => $name,
    'email' => $email,
    'avatar' => $avatarPath,
    'password' => password_hash($password, PASSWORD_DEFAULT)
];

$stmt = $pdo->prepare($query);

try {
    $stmt->execute($params);
} catch (\Exception $e) {
    die($e->getMessage());
}
redirect('/sigIn/index.php');
