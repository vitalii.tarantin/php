<?php

/**
 * Test register and sigIn
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */

require_once __DIR__ . '/src/helpers.php';
checkGuest();
?>

<!DOCTYPE html>
<html lang="en" data-theme="dark">
<?php require_once __DIR__ . '/components/head.php' ?>
<body>
<form action="src/action/register.php" class="card" method="post" enctype="multipart/form-data">
    <h2>Registration</h2>
    <label for="name">
        Name
        <input
                type="text"
                name="name"
                id="name"
                placeholder="Jon Doe"
                value="<?php echo old('name') ?>"
                <?php echo validationErrorAttr('name'); ?>

        >
        <?php if (hasValidationError('name')) : ?>
            <small><?php echo validationErrorMessage('name');?></small>
        <?php endif; ?>
    </label>
    <label for="email">
        E-mail
        <input
            type="email"
            id="email"
            name="email"
            placeholder="example@mail.su"
            value="<?php echo old('email') ?>"
            <?php echo validationErrorAttr('email'); ?>

        >
        <?php if (hasValidationError('email')) : ?>
            <small><?php echo validationErrorMessage('email');?></small>
        <?php endif; ?>
    </label>
    <label for="avatar">
        <input
            type="file"
            id="avatar"
            name="avatar"
            <?php echo validationErrorAttr('avatar'); ?>
        >
        <?php if (hasValidationError('avatar')) : ?>
            <small><?php echo validationErrorMessage('avatar');?></small>
        <?php endif; ?>
    </label>
    <div class="grid">
        <label for="password">
            Password
            <input
                    type="password"
                    id="password"
                    name="password"
                    placeholder="********"
                    <?php echo validationErrorAttr('password'); ?>

            >
            <?php if (hasValidationError('password')) : ?>
                <small><?php echo validationErrorMessage('password');?></small>
            <?php endif; ?>
        </label>
        <label for="password_confirmation">
            Confirmation
            <input
                type="password"
                id="password_confirmation"
                name="password_confirmation"
                placeholder="********"

            >
        </label>
    </div>
    <fieldset>
        <label for="terms">
            <input
                type="checkbox"
                id="terms"
                name="terms"
            >
            I accept all terms of use
        </label>
    </fieldset>
    <button
        type="submit"
        id="submit"
    >Continue</button>
</form>
<p>I already have <a href="../../lesson/lesson02/index.php">account</a></p>
<?php require_once __DIR__ . '/components/script.php' ?>
</body>
</html>
