-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Хост: my_mysql
-- Час створення: Бер 14 2024 р., 08:50
-- Версія сервера: 5.7.44
-- Версія PHP: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `my_data-base`
--

-- --------------------------------------------------------

--
-- Структура таблиці `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `avatar`, `email`) VALUES
(1, 'Bon Djovi', '$2y$10$VEiJbPbuX5KfenKe3Lsq3.omBiwn2vpLoUTPAuGzLFdY/w8DVwBnS', 'Array', 'example@mail.su'),
(4, 'jon doe', '$2y$10$zo7eg62fpWN4Z7XW1pDrfedM7pDnlKWN1lx1/BYflj6oSsZYdlYpm', 'Array', 'jondoe@mail.su'),
(5, 'jon doe', '$2y$10$O.v3Bxc3zsD60CbwJLXHW.F.e/vyOogQu7vtM67.839cEhdHz5di2', 'Array', 'joxdoe@mail.su'),
(6, 'jon doe', '$2y$10$SCvze3jyCyn4cgLUFHKjoeRHSqrdCU6IL18e5sOzb0yIC3qcaHlP2', 'Array', 'joxoe@mail.su'),
(8, 'jon doe', '$2y$10$V2WRJioFZXqoLrVBnyVGtOYPkQGl4gR6/i6985yTpY3wnZzn5kVr2', 'Array', 'joxote@mail.su'),
(10, 'jon doe', '$2y$10$G3KpNE/uOJGyPXPXk2WmIOYzHadJaFUOlw26FerHiNKENUQ64BTqy', 'Array', 'joxohr5tte@mail.su'),
(12, 'jon doe', '$2y$10$3IQCRL9B3fIZGeyTq7T3auf5RhPNpmgy5ITyyj0Pjd9r9KTPJLlCy', 'Array', 'joohr5tte@mail.su'),
(13, 'jon doe', '$2y$10$lp5E/e4YE/0AcyEOqFBEq.vJhaoz7BlYrbLoeJjnlUn/VX.9eDMTq', 'Array', 'joohr.i5tte@mail.su'),
(14, 'jon doe', '$2y$10$VYTl52iB9LZ9hAhnFhCZk.FEWNJQ.ki52XW38vypiHfeYMjHjGLCG', 'Array', 'johr.i5tte@mail.su'),
(15, 'dsbba', '$2y$10$f.K6UbwpNrqY0VfLM28N1eMuaDqZa4SeJnb58fHwopf0JCjX2M1mW', 'Array', 'dbdbe@evh.com'),
(16, 'dsbba', '$2y$10$lWncjben/PiNka6rhiWwluXS2BUs0nkxpO2OEuOity8GkUegQk3oK', 'Array', 'bdbe@evh.com'),
(18, 'dsbba', '$2y$10$mQFQ7y0RwujrNX0Nsj2yJOBivqZxCIkiaqLg1PJYw0HqsFtUs9jBi', 'Array', 'bbe@evh.com'),
(20, 'dsbba', '$2y$10$8hWMrjDOGrNODz9HGGVCvO4zgCblY31.ONSrELn2XDksCEy3LmUdm', 'Array', 'bb@evh.com'),
(22, 'dsbba', '$2y$10$Nt/HbXzKwoDTkV0i4KFbZ.Tmw5opov378CrovVidFJPT6JlsMsqpy', 'Array', 'bib@evh.com'),
(23, 'tyjhtyj', '$2y$10$BVQngVOeDkwe3IRyaq7wHuWZ82XO9B/M1LhO2A6UTb5roqWNlmpr6', 'uploads/avatar_1710240146.jpg', 'ubib@evh.com'),
(24, 'y7y', '$2y$10$g9B3wQCLgWN/gqspdNWNUOmsdTmlpxE7AM6v2Tlp0HbACWoXJMxhq', 'uploads/avatar_1710240209.png', 'ev@mail.cpm'),
(26, 'y7y', '$2y$10$FxZfvH4tY5OCFpRIQpSfYeaEtU3dpK1WDvd0uJIb80dE8fRb.Hwza', 'uploads/avatar_1710240255.png', 'euv@mail.cpm'),
(27, ' homer', '$2y$10$/xMvT3Nny5bGe3Qn8MX0QuIE01imAwJ75QJw2SUi6vrJR3g1k8IU.', 'uploads/avatar_1710254373.jpg', 'homer@gmail.com');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `password` (`password`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
