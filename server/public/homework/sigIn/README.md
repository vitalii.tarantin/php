# Registration and authorization in PHP

The CSS framework used is [picocss](https://picocss.com/).
You can apply a dark or light theme by changing the `data-theme` attribute of the `<html>` tag.

```html
<!-- светлая -->
<html data-theme="light">
<!-- темная -->
<html data-theme="dark">
```
### Database

In the current project, there is already a ready SQL to create the `users` table. Create
database and import `db/users.sql` file into it.

### Configuration

In the configuration file `src/config.php` you can configure the database connection.

- `DB_HOST` - the host on which the database is accessed
- `DB_PORT` - database port
- `DB_NAME` - database name
- `DB_USERNAME` - user login for database management
- `DB_PASSWORD` - password

Note: PHP 8.0+ version is required to run the site

Also for the current project there is a ready-made [Docker](https://www.docker.com/) infrastructure, which will make it easy to launch the application.
It is enough to execute the command:

``shell
docker-compose up
```

- http://localhost:8181 - website
- http://localhost:8088 - PhpMyAdmin

Access to PhpMyAdmin:

- login - `admin`
- password - `qwerty`

In `src/config.php`:

```php
<?php

const DB_HOST = 'db';
const DB_PORT = '3306';
const DB_NAME = '<database name>';
const DB_USERNAME = 'root';
const DB_PASSWORD = 'password';
```

Если возникнет ошибка `Permission denied for mkdir()`, то введите команду:

```shell
sudo chmod 777 src/helpers.php
```
![Registration](https://i.ibb.co/6wtc4XN/Screenshot-from-2024-03-14-11-00-39.png)
![Log In](https://i.ibb.co/N2QRdVz/Screenshot-from-2024-03-14-11-00-50.png)
![Home page](https://i.ibb.co/ZN8jPGz/Screenshot-from-2024-03-14-11-51-35.png)