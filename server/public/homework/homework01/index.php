<?php
/**
 * Homework_01
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */
$name = 'Homer';
$age = 32;
$pi = pi();
$arrayNameOne = ['Vitalii', 'Kseniya', 'Homer'];
$arrayNameTwo = ['Lisa', 'Bart'];
$arrayNameThree = ['Meggi', "Marg"];
$joinArrayOneAndTwo = array($arrayNameOne, $arrayNameTwo);
$joinArrayTwoAndThree = array($arrayNameTwo, $arrayNameThree);
$resultArrayOne = array($arrayNameOne, $joinArrayOneAndTwo);
$resultArrayTwo = array($arrayNameOne, $arrayNameTwo, $arrayNameThree);

echo "<br> 2. создать переменную строку ваше имя и вывести на экран <br>";

echo "My name is $name <br>";
print("My name is $name <br>");
print_r("My name is $name <br>");
echo '<pre>';
var_dump($name);
echo '</pre>';

echo "<br> 3. создать переменную возраст и вывести на экран <br>";

echo "My age is $age <br>";
print("My age is $age <br>");
print_r("My age is $age <br>");




echo "This is PI  = $pi <br>";
print("This is PI  = $pi <br>");
print_r("This is PI  = $pi <br>");




echo '<pre>';
print_r($arrayNameOne);
echo '</pre>';

echo '<pre>';
var_dump($arrayNameOne);
echo '</pre>';



echo '<pre>';
print_r($joinArrayOneAndTwo);
echo '</pre>';

echo '<pre>';
var_dump($joinArrayOneAndTwo);
echo '</pre>';


echo '<pre>';
print_r($resultArrayOne);
echo '</pre>';

echo '<pre>';
var_dump($resultArrayOne);
echo '</pre>';


echo '<pre>';
print_r($resultArrayTwo);
echo '</pre>';

echo '<pre>';
var_dump($resultArrayTwo);
echo '</pre>';