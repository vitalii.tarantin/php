<?php

/**
 * Homework_04 loop do foreach
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */

/**
 * Function getArray
 *
 * @param array $arr your array
 *
 * @return void
 */
function getArrayView(array $arr)
{
    foreach ($arr as $item) {
        echo $item . '<br>';
    }
}
echo '<pre>1. Implement your own count() function</pre>';
/**
 * Function myCountForeach
 *
 * @param array|string $array param array|string
 *
 * @return integer
 */
function myCountForeach(array|string $array): int
{
    $countForeach = 0;
    foreach ($array as $value) {
        $countForeach++;
    }
    return $countForeach;
}
$array = ['I', 'say', 'dou', 'when', 'my', 'app',  'print', 'error'];

echo '<pre>2. An array $arr is given. Expand this array in the reverse direction.</pre>';
$arr = ['I', 'say', 'dou', 'when', 'my', 'app',  'print', 'error'];
$count = myCountForeach($arr) - 1;
$revers = [];
foreach ($arr as $item) {
    $revers[] = $arr[$count];
    $count--;
}
getArrayView($revers);
echo '<pre>3. An array [44, 12, 11, 7, 1, 99, 43, 5, 69] is given. Expand this array in the reverse direction.</pre>';
$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$count = myCountForeach($arr) - 1;
$revers = [];
foreach ($arr as $item) {
    $revers[] = $arr[$count];
    $count--;
}
getArrayView($revers);
print '<pre>4. Given string $str = hello world because I love stereotypes!'
    . 'Expand the string in the reverse direction.</pre>';
print '<br>This impossible foreach construct only works with arrays and objects<br>';
print '<pre>5. Given a string. ready function toUpperCase() or tolowercase()'
    . ' $str = hello world because I love stereotypes!, make it with small letters.</pre>';
print '<br>This impossible foreach construct only works with arrays and objects';
print '<pre>6. Given string $str = hello world because I love stereotypes!, make all letters large..</pre>';
print '<br>This impossible foreach construct only works with arrays and objects';
print '<pre>7. Given an array [Alex, Vanya, Tanya, Lena, Tolya], make all letters small.</pre>';
$arr = ['I', 'sAy', 'dou', 'when', 'my', 'aPp',  'print', 'errOr'];
$arrLower = [];
foreach ($arr as $item) {
    $arrLower[] = strtolower($item);
}
getArrayView($arrLower);
print '<pre>8. Given array [Alex, Vanya, Tanya, Lena, Tolya], make all letters capitalized.</pre>';
$arrUpper = [];
foreach ($arr as $item) {
    $arrUpper[] = strtoupper($item);
}
getArrayView($arrUpper);
print '<pre>9. Given a number $num = 1234678, expand it in the reverse direction.</pre>';
print '<br>This impossible foreach construct only works with arrays and objects<br>';

print '<pre>10. An array [44, 12, 11, 7, 1, 99, 43, 5, 69] is given, sort it in descending order.</pre>';
$arrNumber = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$arrReversNumber = $arrNumber;
$length = myCountForeach($arrReversNumber);
foreach ($arrNumber as $i) {
    foreach ($arrNumber as $j => $i) {
        if ($j === $length - 1) {
            break;
        }
        $k = $j + 1;
        if ($arrReversNumber[$k] > $arrReversNumber[$j]) {
            [ $arrReversNumber[$k], $arrReversNumber[$j] ] = [ $arrReversNumber[$j], $arrReversNumber[$k] ];
        }
    }
}
getArrayView($arrReversNumber);
