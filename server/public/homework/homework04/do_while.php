<?php

/**
 * Homework_04 loop do while
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */

/**
 * Function for get array
 *
 * @param array $arr array
 *
 * @return void
 */
function getArrayView(array $arr)
{
    foreach ($arr as $item) {
        echo $item . '<br>';
    }
}
echo '<pre>1. Implement your own count() function</pre>';
/**
 * Function
 *
 * @param array|string $arr bla bla
 *
 * @return integer
 */
function myCountDoWhile(array|string $arr): int
{
    $count = 0;
    do {
        $count++;
    } while (isset($arr[$count]));
    return $count;
}
$arr = ['I', 'say', 'dou', 'when', 'my', 'app',  'print', 'error'];
print (myCountDoWhile($arr));
echo '<pre>2. An array $arr is given. Expand this array in the reverse direction.</pre>';
$i = myCountDoWhile($arr) - 1;
$revers = [];
do {
    $revers[] = $arr[$i];
    $i--;
} while (isset($arr[$i]));
getArrayView($revers);
echo '<pre>3. An array [44, 12, 11, 7, 1, 99, 43, 5, 69] is given. Expand this array in the reverse direction.</pre>';
$arrayNumber = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$i = myCountDoWhile($arrayNumber) - 1;
$reversNumber = [];
do {
    $reversNumber[] = $arrayNumber[$i];
    $i--;
} while (isset($arrayNumber[$i]));
getArrayView($reversNumber);
echo '<pre>4. Given string $str = hello world because I love stereotypes!'
    . 'Expand the string in the reverse direction.</pre>';
$str = 'hello world because I love stereotypes!';
$i = myCountDoWhile($str);
$strRevers = '';
do {
    $i--;
    $strRevers .= $str[$i];
} while ($i >= 0);
print($strRevers);
echo '<pre>5. Given a string. ready function toUpperCase() or tolowercase() '
    . '$str = hello world because I love stereotypes!, make all letters large.</pre>';
/**
 * Function UpperCase
 *
 * @param array $str your string
 *
 * @return string
 */
function upperCaseMap(array $str): string
{
    $upper_case_map = [
        'a' => 'A',
        'b' => 'B',
        'c' => 'C',
        'd' => 'D',
        'e' => 'E',
        'f' => 'F',
        'g' => 'G',
        'h' => 'H',
        'i' => 'I',
        'j' => 'J',
        'k' => 'K',
        'l' => 'L',
        'm' => 'M',
        'n' => 'N',
        'o' => 'O',
        'p' => 'P',
        'q' => 'Q',
        'r' => 'R',
        's' => 'S',
        't' => 'T',
        'u' => 'U',
        'v' => 'V',
        'w' => 'W',
        'x' => 'X',
        'y' => 'Y',
        'z' => 'Z',
    ];
    $i = 0;
    $upperSpring = '';
    do {
        $upperSpring .= $upper_case_map[$str[$i]] ?? $str[$i];
        $i++;
    } while (isset($str[$i]));
    return $upperSpring;
}

echo '<pre>6. Given string $str = HELLO WORLD BECAUSE I LOVE STEREOTYPES!, make it with small letters.</pre>';
/**
 * Function lowerCase
 *
 * @param array $str your arr
 *
 * @return string
 */
function loverCaseMap(array $str): string
{
    $lover_case_map = [
        'A' => 'a',
        'B' => 'b',
        'C' => 'c',
        'D' => 'd',
        'E' => 'e',
        'F' => 'f',
        'G' => 'g',
        'H' => 'h',
        'I' => 'i',
        'J' => 'j',
        'K' => 'k',
        'L' => 'l',
        'M' => 'm',
        'N' => 'n',
        'O' => 'o',
        'P' => 'p',
        'Q' => 'q',
        'R' => 'r',
        'S' => 's',
        'T' => 't',
        'U' => 'u',
        'V' => 'v',
        'W' => 'w',
        'X' => 'x',
        'Y' => 'y',
        'Z' => 'z',
    ];
    $i = 0;
    $stringUpper = '';
    do {
        $stringUpper .= $lover_case_map[$str[$i]] ?? $str[$i];
        $i++;
    } while (isset($str[$i]));
    return $stringUpper;
}
$stringLover = 'HELLO WORLD! BECAUSE I LOVE STEREOTYPES!';

echo '<pre>7. Given array [Alex, Vanya, Tanya, Lena, Tolya], make all letters capitalized.</pre>';

$arrayName = ['I', 'sAy', 'dOu', 'wHEn', 'mY', 'aPp',  'pRInt', 'errOr'];
$i = 0;
do {
    $arrayUpper[] = strtoupper($arrayName[$i]);
    $i++;
} while ($i < myCountDoWhile($arrayName));
getArrayView($arrayUpper);
echo '<pre>8. Given an array [Alex, Vanya, Tanya, Lena, Tolya], make all letters small.</pre>';
$i = 0;
do {
    $arrayName[$i] = strtolower($arrayName[$i]);
    $i++;
} while ($i < myCountDoWhile($arrayName));
getArrayView($arrayName);
echo '<pre>9. Given a number $num = 1234678, expand it in the reverse direction.</pre>';
$number = 123456789;
$string = strval($number);
$count = myCountDoWhile($string) - 1;
$newString = '';
do {
    $newString .= $string[$count];
    $count--;
} while ($count >= 0);
  $newString = (int)$newString;

echo '<pre>10. An array [44, 12, 11, 7, 1, 99, 43, 5, 69] is given, sort it in descending order.</pre>';

$arrayIncrease = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$i = 0;
do {
    $i++;
    $j = 0;
    do {
        $k = $j + 1;
        if ($arrayIncrease[$k] > $arrayIncrease[$j]) {
            [ $arrayIncrease[$k], $arrayIncrease[$j] ] = [$arrayIncrease[$j], $arrayIncrease[$k]];
        }
        $j++;
    } while ($j < myCountDoWhile($arrayIncrease) - 1);
} while ($i < myCountDoWhile($arrayIncrease));
getArrayView($arrayIncrease);
