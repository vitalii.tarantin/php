<?php

/**
 * Homework_04 loop for
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */

/**
 * Function getArray
 *
 * @param array $arr param array
 *
 * @return void
 */
function getArrayView(array $arr)
{
    foreach ($arr as $item) {
        echo $item . '<br>';
    }
}
echo '<pre>1. Implement your own count() function</pre>';
/**
 * Function myCount
 *
 * @param array $array param array
 *
 * @return integer
 */
function myCount(array $array)
{
    $count = 0;
    for ($i = 0;; $i++) {
        if (isset($array[$i])) {
            $count++;
        } else {
            break;
        }
    }
    return $count;
}

/**
 * Function myCountString
 *
 * @param string|array $string param array
 *
 * @return integer
 */
function myCountString(string|array $string): int
{
    $count = 0;
    for ($i = 0;; $i++) {
        if (isset($string[$i])) {
            $count++;
        } else {
            break;
        }
    }
    return $count;
}
echo '<pre>2. An array $arr is given. Expand this array in the reverse direction.</pre>';
$arr = ['I', 'say', 'dou', 'when', 'my', 'app',  'print', 'error'];
$revers = [];
$length = myCount($arr);
for ($i = $length - 1; $i >= 0; $i--) {
     $revers[] = $arr[$i];
}
 getArrayView($revers);
echo '<pre>3. An array [44, 12, 11, 7, 1, 99, 43, 5, 69] is given. Expand this array in the reverse direction.</pre>';
$arrayNumber = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$reversNumber = [];
$length = myCount($arrayNumber);
for ($i = $length - 1; $i >= 0; $i--) {
    $reversNumber[] = $arrayNumber[$i];
}
getArrayView($reversNumber);
echo '<pre>4. Given string $str = hello world because I love stereotypes!'
    . 'Expand the string in the reverse direction.</pre>';
$string = 'hello world because I love stereotypes!';
$stringRevers = '';
$length = myCountString($string);
for ($i = $length - 1; $i >= 0; $i--) {
    $stringRevers .= $string[$i];
}
echo '<pre>';
echo $stringRevers;
echo '</pre>';

echo '<pre>5. Given a string. ready function toUpperCase() or tolowercase()'
    . '$str = hello world because I love stereotypes!, make it with small letters.</pre>';
/**
 * Function my_upper_case
 *
 * @param string|array $string param array
 *
 * @return string
 */
function myUpperCase(string|array $string): string
{
    $upper_case_map = [
        'a' => 'A',
        'b' => 'B',
        'c' => 'C',
        'd' => 'D',
        'e' => 'E',
        'f' => 'F',
        'g' => 'G',
        'h' => 'H',
        'i' => 'I',
        'j' => 'J',
        'k' => 'K',
        'l' => 'L',
        'm' => 'M',
        'n' => 'N',
        'o' => 'O',
        'p' => 'P',
        'q' => 'Q',
        'r' => 'R',
        's' => 'S',
        't' => 'T',
        'u' => 'U',
        'v' => 'V',
        'w' => 'W',
        'x' => 'X',
        'y' => 'Y',
        'z' => 'Z',
    ];
    $stringUpper = '';
    for ($i = 0; isset($string[$i]); $i++) {
        $stringUpper .= $upper_case_map[$string[$i]] ?? $string[$i];
    }
    return $stringUpper;
}
echo '<pre>6. Given string $str = hello world because I love stereotypes!, make all letters large..</pre>';
/**
 * Function myLoverCase
 *
 * @param array|string $string param array
 *
 * @return string
 */
function myLoverCase(array|string $string): string
{
    $lover_case_map = [
        'A' => 'a',
        'B' => 'b',
        'C' => 'c',
        'D' => 'd',
        'E' => 'e',
        'F' => 'f',
        'G' => 'g',
        'H' => 'h',
        'I' => 'i',
        'J' => 'j',
        'K' => 'k',
        'L' => 'l',
        'M' => 'm',
        'N' => 'n',
        'O' => 'o',
        'P' => 'p',
        'Q' => 'q',
        'R' => 'r',
        'S' => 's',
        'T' => 't',
        'U' => 'u',
        'V' => 'v',
        'W' => 'w',
        'X' => 'x',
        'Y' => 'y',
        'Z' => 'z',
    ];
    $stringLover = '';
    for ($i = 0; isset($string[$i]); $i++) {
        $stringLover .= $lover_case_map[$string[$i]] ?? $string[$i];
    }
    return $stringLover;
}
$stringLover = 'HELLO WORLD BECAUSE I LOVE STEREOTYPES!';
echo '<pre>7. Given an array [Alex, Vanya, Tanya, Lena, Tolya], make all letters small.</pre>';
$arr = ['I', 'sAy', 'dou', 'when', 'my', 'aPp',  'print', 'errOr'];
for ($i = 0; $i < myCount($arr); $i++) {
    $arr[$i] = strtolower($arr[$i]);
}

echo '<pre>8. Given array [Alex, Vanya, Tanya, Lena, Tolya], make all letters capitalized.</pre>';
for ($i = 0; $i < myCount($arr); $i++) {
    $arr[$i] = strtoupper($arr[$i]);
}

echo '<pre>9. Given a number $num = 1234678, expand it in the reverse direction.</pre>';
$number = 1234678;
$number_string = strval($number);
$newInt = '';
for ($i = myCountString($number_string) - 1; $i >= 0; $i--) {
     $newInt .= $number_string[$i];
}
$newInt = (int)$newInt;
echo '<pre>10. An array [44, 12, 11, 7, 1, 99, 43, 5, 69] is given, sort it in descending order.</pre>';

$arrayIncrease = [44, 12, 11, 7, 1, 99, 43, 5, 69];
for ($i = 0; $i < myCount($arrayIncrease); $i++) {
    for ($j = 0; $j < myCount($arrayIncrease) - 1; $j++) {
        $k = $j + 1;
        if ($arrayIncrease[$k] > $arrayIncrease[$j]) {
            [ $arrayIncrease[$k], $arrayIncrease[$j] ] = [ $arrayIncrease[$j], $arrayIncrease[$k] ];
        }
    }
}
getArrayView($arrayIncrease);

echo "<pre>"; // Додаємо тег <pre> для збереження форматування
$n = 5; // Висота трикутника
for ($i = 1; $i <= $n; $i++) {
    // Вивід пробілів для вирівнювання
    for ($j = $i; $j < $n; $j++) {
        echo " ";
    }
    // Вивід зірочок і пробілів
    for ($j = 1; $j <= (2 * $i - 1); $j++) {
        if ($j == 1 || $j == (2 * $i - 1) || $i == $n) {
            echo "*"; // Зірочки по краях або на останньому рядку
        } else {
            echo " "; // Пробіли всередині
        }
    }
    // Перехід на новий рядок
    echo "<br>";
}
echo "</pre>";
echo '<br>' .'-------------------------------------------------' . '<br>';
echo "<pre>";
$n = 5;
for ($i = 1; $i <= $n; $i++) {
    for ($j = 1; $j <= $n; $j++) {
        if ($j == $i || $j == $n - $i + 1) {
            echo "*";
        } else {
            echo " ";
        }
    }
    echo "<br>";
}
echo "</pre>";
echo '<br>' .'-------------------------------------------------' . '<br>';
$n = 5;
for ($i = 1; $i <= $n; $i++) {
    for ($j = 1; $j <= $i; $j++) {
        echo "*";
    }
    echo " " . '<br>';
}
echo '<br>' .'-------------------------------------------------' . '<br>';

$n = 5;
for ($i = $n; $i >= 1; $i--) {
    for ($j = 1; $j <= $i; $j++) {
        echo "*";
    }
    echo " " . '<br>';
}
echo '<br>' .'-------------------------------------------------' . '<br>';

echo "<pre>";
$n = 5;
for ($i = 1; $i <= $n; $i++) {
    for ($j = 1; $j <= $n; $j++) {
        if ($i == 1 || $i == $n || $j == 1 || $j == $n) {
            echo "*";
        } else {
            echo " ";
        }
    }
    echo "<br>";
}
echo "</pre>";
echo '<br>' .'-------------------------------------------------' . '<br>';
echo "<pre>";
$n = 5;

for ($i = 1; $i <= $n; $i++) {
    for ($j = 1; $j <= $i; $j++) {
        echo "*";
    }
    echo "<br>";
}
for ($i = $n - 1; $i >= 1; $i--) {
    for ($j = 1; $j <= $i; $j++) {
        echo "*";
    }
    echo "<br>";
}
echo "</pre>";
echo '<br>' .'-------------------------------------------------' . '<br>';
echo "<pre>";
$n = 5;
for ($i = 1; $i <= $n; $i++) {
    for ($j = $i; $j < $n; $j++) {
        echo " ";
    }
    for ($j = 1; $j <= (2 * $i - 1); $j++) {
        echo "*";
    }
    echo "\n";
}
echo "</pre>";

echo '<br>' .'-------------------------------------------------' . '<br>';
echo "<pre>";
$n = 5;
for ($i = $n; $i >= 1; $i--) {
    for ($j = $i; $j < $n; $j++) {
        echo " ";
    }
    for ($j = 1; $j <= (2 * $i - 1); $j++) {
        echo "*";
    }
    echo "\n";
}
echo "</pre>";

echo '<br>' .'-------------------------------------------------' . '<br>';
echo "<pre>";
$n = 5;
for ($i = 1; $i <= $n; $i++) {
    for ($j = $i; $j < $n; $j++) {
        echo " ";
    }
    for ($j = 1; $j <= (2 * $i - 1); $j++) {
        echo "*";
    }
    echo "\n";
}
for ($i = $n; $i >= 1; $i--) {
    for ($j = $i; $j < $n; $j++) {
        echo " ";
    }
    for ($j = 1; $j <= (2 * $i - 1); $j++) {
        echo "*";
    }
    echo "\n";
}
echo "</pre>";

echo '<br>' .'-------------------------------------------------' . '<br>';
echo "<pre>";
$n = 6;
for ($i = $n / 2; $i <= $n; $i += 2) {
    for ($j = 1; $j < $n - $i; $j += 2) {
        echo " ";
    }
    for ($j = 1; $j <= $i; $j++) {
        echo "*";
    }
    for ($j = 1; $j <= $n - $i; $j++) {
        echo " ";
    }
    for ($j = 1; $j <= $i; $j++) {
        echo "*";
    }
    echo "\n";
}
for ($i = $n; $i >= 1; $i--) {
    for ($j = $i; $j < $n; $j++) {
        echo " ";
    }
    for ($j = 1; $j <= ($i * 2) - 1; $j++) {
        echo "*";
    }
    echo "\n";
}
echo "</pre>";
