<?php

/**
 * Homework_04 loop do unit_02
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */

/**
 * Function getArrayView
 *
 * @param array $arr param array
 *
 * @return void
 */
function getArrayView(array $arr)
{
    foreach ($arr as $item) {
        echo $item . '<br>';
    }
}
echo '<pre>1. Write a program that outputs prime numbers, i.e. dividing without remainder only by itself and by 1.</pre>';
$limit = 100;
echo sprintf('Prime numbers up to %s are: ', $limit);
for ($num = 2; $num <= $limit; $num++) {
    $is_prime = true;
    for ($i = 2; $i <= sqrt($num); $i++) {
        if ($num % $i == 0) {
            $is_prime = false;
            break;
        }
    }
    if ($is_prime) {
        echo $num . ' ';
    }
}
echo '<pre>2. Згенеруйте 100 разів нове число і виведіть на екран кількість парних чисел із цих 100..</pre>';
$arrayEven = [];
$arraySum = [0];
for ($i = 0; $i < 100; $i++) {
    $numberEven = rand(1, 100);
    if ($numberEven % 2 == 0) {
        $arrayEven[] = $numberEven;
        $arraySum[0]++;
    }
}
getArrayView($arrayEven);
echo '<br> Total number of even numbers - ';
getArrayView($arraySum);
echo '<pre>3. Згенеруйте 100 разів число від 1 до 5 і виведіть на екран скільки разів згенерувалися ці числа (1, 2, 3, 4 і 5).</pre>';

$arraySumNumber = [0, 0, 0, 0, 0];
for ($i = 1; $i <= 100; $i++) {
    $randNumber = rand(1, 5);
    switch ($randNumber) {
    case 1:
        $arraySumNumber[0]++;
        break;
    case 2:
        $arraySumNumber[1]++;
        break;
    case 3:
        $arraySumNumber[2]++;
        break;
    case 4:
        $arraySumNumber[3]++;
        break;
    case 5:
        $arraySumNumber[4]++;
        break;
    }
}
getArrayView($arraySumNumber);
echo '<pre>4. Використовуючи умови та цикли зробити таблицю в 5 колонок і 3 рядки (5x3), відзначити різними кольорами частину комірок.</pre>';
print "<table border='1'";
for ($i = 0; $i < 3; $i++) {
    echo '<tr>';
    for ($j = 0; $j < 5; $j++) {
        $a = rand(1, 256);
        $b = rand(1, 256);
        $c = rand(1, 256);
        $format = '<td style=background-color:rgb(%s,%d,%e)>I say dou when my app wright error!</td>';
        echo sprintf($format, $a, $b, $c);
    }
    echo '</tr>';
}
echo '</table>';

echo '<pre>5. У змінній month лежить якесь число з інтервалу від 1 до 12. Визначте, у яку пору року потрапляє цей місяць (зима, літо, весна, осінь).</pre>';
$month = rand(1, 12);
if ($month == 12 || $month == 1 || $month == 2) {
     echo '<img src="img/winter.jpg" alt="#">';
} elseif ($month == 3 || $month == 4 || $month == 5) {
     echo '<img src="img/spring.webp" alt="#">';
} elseif ($month == 6 || $month == 7 || $month == 8) {
     echo '<img src="img/summer.jpg" alt="summer">';
} elseif ($month == 9 || $month == 10 || $month == 11) {
     echo '<img src="img/autumn.jpg" alt="#">';
}
echo '<pre>6. Дано рядок, що складається із символів, наприклад, abcde. Перевірте, що першим символом цього рядка є буква a. Якщо це так - виведіть так, в іншому випадку виведіть ні.</pre>';
$stringLetter = 'bbcde';
if ($stringLetter[0] == 'a') {
    echo $stringLetter . ' - first letter a';
} else {
    echo $stringLetter . ' - first letter b';
}
echo '<pre> 7. Дано рядок із цифрами, наприклад, 12345. Перевірте, що першим символом цього рядка є цифра 1, 2 або 3. Якщо це так - виведіть так, в іншому випадку виведіть ні.</pre>';
$stringNumber = rand(10000, 99999);
$stringNumber .= '';
if ($stringNumber[0] == 1 || $stringNumber[1] == 2 || $stringNumber[2] == 3) {
    echo '<p>hello world! because I LOVE stereotypes</p>';
} else {
    echo '<p>hello world!</p>';
}
echo '<pre> 8. Якщо змінна test дорівнює true, то виведіть Вірно, інакше виведіть Невірно. Перевірте роботу скрипта при test, що дорівнює true, false. Напишіть два варіанти скрипта - тернарка і if else..</pre>';
$test = rand(0, 1);
if ($test) {
    echo 'variable $test = true';
} else {
    echo 'variable $test = false';
}
echo '<br>';
echo $test ? 'variable $test = true' : 'variable $test = false';
echo '<pre> 9. Дано Два масиви рус і англ [пн, вт, ср, чт, пт, сб, вс]. Якщо змінна lang = ru вивести масив російською мовою, а якщо en то вивести англійською мовою. Зробити через if else і через тернарку.</pre>';
$arrayLang = ['ua', 'en'];
$lang = array_rand($arrayLang, 1);
$langUkrainian = ['пон', 'вів', 'сер', 'чет', 'пят', 'сб', 'нед'];
$langEnglish = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];

if ($lang == 0) {
    getArrayView($langUkrainian);
} else {
    getArrayView($langEnglish);
}
echo '<pre>';
echo '</pre>';
$lang == 0 ? getArrayView($langEnglish) : getArrayView($langUkrainian);
echo '<pre> 10. У змінній сlock лежить число від 0 до 59 - це хвилини. Визначте, у яку чверть години потрапляє це число (у першу, другу, третю або четверту). тернарка та if else.</pre>';

$clock = rand(0, 59);
if ($clock <= 15) {
     echo $clock . 'minutes Fist quarter of an hour';
} elseif ($clock <= 30) {
     echo $clock . 'minutes Second quarter of an hour';
} elseif ($clock <= 45) {
     echo $clock . 'minutes Third quarter of an hour';
} elseif ($clock <= 60) {
     echo $clock . 'minutes Fourth quarter of an hour';
}

 echo '<pre>';
 echo 'Example switch case';
 echo '</pre>';

switch ($clock) {
case ($clock <= 15):
    echo $clock . 'minutes Fist quarter of an hour switch case';
    break;
case ($clock <= 30):
    echo $clock . 'minutes Second quarter of an hour switch case';
    break;
case ($clock <= 45):
    echo $clock . 'minutes Third quarter of an hour switch case';
    break;
case ($clock <= 60):
    echo $clock . 'minutes Fourth quarter of an hour switch case';
    break;
default:
    echo 'number is not odd number';
}
echo '<pre>';
echo 'Ternary surgery';
echo '</pre>';

$half = $clock <= 15 ? print $clock . 'minutes Fist quarter of an hour ternary surgery' : (($clock <= 30) ? print $clock . 'minutes Third quarter of an hour ternary surgery' : (($clock <= 45) ? print $clock . 'minutes Second quarter of an hour ternary surgery' : (($clock <= 60 ? $clock . 'minutes Fourth quarter of an hour ternary surgery' : print 'number is not odd number'))));
