<?php

/**
 * Homework_04 loop while
 * PHP version 8.0.20 (cli) (built: Jun 23 2022 08:36:55) ( NTS )
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181
 */


/**
 * Function getArrayView loop while
 *
 * @param array $arr param array
 *
 * @return void
 */
function getArrayView(array $arr)
{
    foreach ($arr as $item) {
        echo $item . '<br>';
    }
}
echo '<pre>1. Implement your own count() function</pre>';
/**
 * Function myCountWhile
 *
 * @param array|string $arr param array|string
 *
 * @return integer
 */
function myCountWhile(array|string $arr): int
{
    $count = 0;
    while (isset($arr[$count])) {
        $count++;
    }
    return $count;
}
$arr = ['I', 'say', 'dou', 'when', 'my', 'app',  'print', 'error'];
print (myCountWhile($arr));
echo '<pre>2. An array $arr is given. Expand this array in the reverse direction.</pre>';

$i = myCountWhile($arr) - 1;
$revers = [];
while (isset($arr[$i])) {
    $revers[] = $arr[$i];
    $i--;
}
echo '<pre>3. An array [44, 12, 11, 7, 1, 99, 43, 5, 69] is given. Expand this array in the reverse direction.</pre>';
$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$i = myCountWhile($arr) - 1;
$revers = [];
while (isset($arr[$i])) {
    $revers[] = $arr[$i];
    $i--;
}
getArrayView($revers);
echo '<pre>4. Given string $str = hello world because I love stereotypes!'
    . 'Expand the string in the reverse direction.</pre>';
$str = 'hello world! because I love stereotypes!';
$i = myCountWhile($str) - 1;
$strRevers = '';
while ($i >= 0) {
    $strRevers .= $str[$i];
    $i--;
}

echo '<pre>5. Given a string. ready function toUpperCase() or tolowercase()'
    . '$str = hello world because I love stereotypes!, make all letters large.</pre>';
/**
 * Function myUpperCase
 *
 * @param string|array $string param array
 *
 * @return string
 */
function myUpperCase(string $string): string
{
    $upper_case_map = [
        'a' => 'A',
        'b' => 'B',
        'c' => 'C',
        'd' => 'D',
        'e' => 'E',
        'f' => 'F',
        'g' => 'G',
        'h' => 'H',
        'i' => 'I',
        'j' => 'J',
        'k' => 'K',
        'l' => 'L',
        'm' => 'M',
        'n' => 'N',
        'o' => 'O',
        'p' => 'P',
        'q' => 'Q',
        'r' => 'R',
        's' => 'S',
        't' => 'T',
        'u' => 'U',
        'v' => 'V',
        'w' => 'W',
        'x' => 'X',
        'y' => 'Y',
        'z' => 'Z',
    ];
    $stringUpper = '';
    $i = 0;
    while (isset($string[$i])) {
        $stringUpper .= $upper_case_map[$string[$i]] ?? $string[$i];
        $i++;
    }
    return $stringUpper;
}
myUpperCase($str);
echo '<pre>6. Given string $str = HELLO WORLD BECAUSE I LOVE STEREOTYPES!, make it with small letters.</pre>';
/**
 * Function myLoverCase
 *
 * @param array|string $string param array
 *
 * @return string
 */
function myLoverCase(array|string $string): string
{
    $lover_case_map = [
        'A' => 'a',
        'B' => 'b',
        'C' => 'c',
        'D' => 'd',
        'E' => 'e',
        'F' => 'f',
        'G' => 'g',
        'H' => 'h',
        'I' => 'i',
        'J' => 'j',
        'K' => 'k',
        'L' => 'l',
        'M' => 'm',
        'N' => 'n',
        'O' => 'o',
        'P' => 'p',
        'Q' => 'q',
        'R' => 'r',
        'S' => 's',
        'T' => 't',
        'U' => 'u',
        'V' => 'v',
        'W' => 'w',
        'X' => 'x',
        'Y' => 'y',
        'Z' => 'z',
    ];
    $stringLover = '';
    $i = 0;
    while (isset($string[$i])) {
        $stringLover .= $lover_case_map[$string[$i]] ?? $string[$i];
        $i++;
    }
    return $stringLover;
}
$stringLover = 'HELLO WORLD! BECAUSE I LOVE STEREOTYPES!';
myLoverCase($stringLover);
echo '<pre>7. Given an array [Alex, Vanya, Tanya, Lena, Tolya], make all letters small.</pre>';

$arr = ['I', 'sAy', 'dou', 'when', 'my', 'aPp',  'print', 'errOr'];
$i = 0;
while ($i < myCountWhile($arr)) {
    $arr[$i] = strtolower($arr[$i]);
    $i++;
}

echo '<pre>8. Given array [Alex, Vanya, Tanya, Lena, Tolya], make all letters capitalized.</pre>';

$arr = ['I', 'sAy', 'dou', 'when', 'my', 'aPp',  'print', 'errOr'];
$i = 0;
while ($i < myCountWhile($arr)) {
    $arr[$i] = strtoupper($arr[$i]);
    $i++;
}

echo '<pre>9. Given a number $num = 1234678, expand it in the reverse direction.</pre>';
$number = 1234567890;
$stringNumber = strval($number);
$newString = '';
$i = myCountWhile($stringNumber) - 1;
while ($i >= 0) {
    $newString .= $stringNumber[$i];
    $i--;
}
$newString = (int)$newString;
echo '<pre>10. An array [44, 12, 11, 7, 1, 99, 43, 5, 69] is given, sort it in descending order.</pre>';

$arrayIncrease = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$i = 0;
while ($i < myCountWhile($arrayIncrease)) {
    $i++;
    $j = 0;
    while ($j < myCountWhile($arrayIncrease) - 1) {
        $k = $j + 1;
        if ($arrayIncrease[$k] > $arrayIncrease[$j]) {
            [ $arrayIncrease[$k], $arrayIncrease[$j] ] = [ $arrayIncrease[$j], $arrayIncrease[$k] ];
        }
        $j++;
    }
}
getArrayView($arrayIncrease);
