<?php

/**
 * Test PHPCS method.
 * php version 8.0
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181/filter.php
 */

echo '<pre>1. Cтворіть три змінні - година, хвилина, секунда. З їхньою допомогою виведіть поточний час у форматі година:хвилина:секунда</pre>';
date_default_timezone_set('Europe/Kiev');
$hour = date('h');
$minute = date(' i');
$second = date(' s');
$time = $hour . $minute . $second;
$time = mb_split(' ', $time);
$result = implode(':', $time);

echo $result;
echo '<br>';
echo date('l jS \of F Y h:i:s A');
echo '<pre>2. Переробіть цей код так, щоб у ньому використовувалася операція .=. Кількість рядків коду при цьому не повинна змінитися!</pre>';
$text = 'Я';
$text .= ' хочу';
$text .= ' знать';
$text .= ' PHP!';
echo $text;
echo '<pre>3. Задано змінну $foo = bar; Створити змінну bar, у якій зберігатиметься число 10. Вивести число 10, використовуючи тільки змінну $foo</pre>';

$foo = 'bar';
$bar = 100;
echo $$foo;
echo '<pre>4. Який буде результат якщо: </pre>';
$a = 2;
$b = 4;
echo $a + $b . '<br>';
echo $a + ++$b . '<br>';
echo $a + $b;
echo '<pre>5. Функції для перевірки типів даних. Визначте що у вас у змінній. Рішення має бути універсальним. Використовуйте тернарну операцію </pre>';

$string = 'hello world because I love stereotypes';

echo isset($string) ? 'змінна існує' : 'змінна не існує';
echo '<br>';

echo gettype($string);
echo '<br>';

echo is_null($string) ? 'null' : 'not null';
echo '<br>';

echo empty($string) ? 'пуста' : 'не пуста';
echo '<br>';

echo is_integer($string) ? 'number' : 'not number';
echo '<br>';

echo is_double($string) ? 'double' : 'not double';
echo '<br>';

echo is_string($string) ? 'змінна рядок' : 'змінна не рядок';
echo '<br>';

echo is_numeric($string) ? 'numeric' : 'not numeric';
echo '<br>';

echo is_bool($string) ? 'true' : 'false';
echo '<br>';

echo is_scalar($string) ? 'scalar' : 'not scalar';
echo '<br>';

echo is_array($string) ? 'array' : 'not array';
echo '<br>';

echo is_object($string) ? 'object' : 'not object';
echo '<br>';
