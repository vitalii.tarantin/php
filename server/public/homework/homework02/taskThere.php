<?php
/**
 * Test PHPCS method.
 * php version 8.0
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181/filter.php
 */
echo '<pre>1. Дано два числа. Знайти їхню суму та добуток</pre>';
$numberOne = 42;
$numberTwo = 23;

echo '<pre>';
echo $numberOne + $numberTwo . '<br>';
echo $numberOne * $numberTwo . '<br>';
echo '</pre>';

echo '<pre>2. Дано два числа. Знайдіть суму їхніх квадратів.</pre>';

echo '<pre>';
echo pow($numberOne, 2) + pow($numberTwo, 2);
echo '</pre>';

echo '<pre>3. Дано три числа. Знайдіть їхнє середнє арифметичне.</pre>';
$numberThree = 22;
echo '<pre>';
echo ($numberOne + $numberTwo + $numberThree) / 3;
echo '</pre>';

echo '<pre>4. Даны три числа x,y и z. Найдите (x+1)−2(z−2x+y)</pre>';
$x = 42;
$y = 23;
$z = 22;
echo ($x + 1) - 2 * ($z - 2 * $x + $y);
echo '</pre>';
echo '<pre>5. Дано натуральне число. Знайдіть залишки від ділення цих чисел на 3 і на 5. Дано число. Збільшіть його на 30%, на 120%</pre>';
$number = 9;
echo '<pre>';

echo ($number % 3) . '<br>';
echo ($number % 5) . '<br>';
$number = 42;
echo 'ЗБІЛЬШЕНЕ ЧИСЛО НА 30% = ' . ($number + $number * 0.3) . '<br>';
echo 'ЗБІЛЬШЕНЕ ЧИСЛО НА 120% = ' . ($number + $number * 1.2) . '<br>';
echo '</pre>';
echo '<pre>6. Дано два числа. Знайдіть суму 40% від першого числа і 84% від другого числа. Дано тризначне число. Знайдіть суму його цифр.</pre>';
$numberOne = 400;
$numberTwo = 200;
echo '<pre>';
echo ($numberOne * 0.4) + ($numberTwo * 0.84);
echo '</pre>';
$numberThree = 987;
$result = str_split($numberThree);
echo '<pre>';
echo ($result[0] + $result[1] + $result[2]) . '<br>';
echo array_sum($result);
echo '</pre>';

echo '<pre>7. Дано тризначне число. Поміняйте середню цифру на нуль. Знайдіть число, отримане виписуванням у зворотному порядку цифр даного тризначного натурального числа</pre>';

$number = 765;
$string = strval($number);
$string[1] = '0';
echo '<pre>';
//var_dump($string);
echo $string[2] . $string[1] . $string[0] . '<br>';
echo strrev($string);
echo '</pre>';

echo '<pre>8. задача ділення за модулем. визначити яке число парне чи непарне. Реалізувати через тернарну операцію.</pre>';
$numberOne = 42;
$numberTwo = 43;
echo '<pre>';
echo $numberOne % 2 == 0 ? 'число парне' : 'число не парне';
echo '<br>';
echo $numberTwo % 2 == 0 ? 'число парне' : 'число не парне';
echo '</pre>';
