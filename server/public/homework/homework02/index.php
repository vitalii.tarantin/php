<?php
/**
 * Test PHPCS method.
 * php version 8.0
 *
 * @category ExampleCategory
 * @package  MyPackage
 * @author   Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://localhost:8181/filter.php
 */
echo '<pre> 1. Дано два числа 42 і 55 визначте за допомогою тернарної операції яке число більше.</pre>';
$firstNumber = 42;
$secondNumber = 55;
$roboto = $firstNumber > $secondNumber ? sprintf(' %s - first більше за %d - second', $firstNumber, $secondNumber) : sprintf(' %d - second більше за %s - first', $secondNumber, $firstNumber);
echo $roboto;


echo '<pre> 2. Використовуй замість статичних чисел функцію rand() для завдання (1); Приклад я обмежив набір випадкових чисел від 5 до 15 echo rand(5, 15);</pre>';

$firstNumber = rand(5, 15);
$secondNumber = rand(5, 15);
$roboto = $firstNumber > $secondNumber ? sprintf(' %s - first більше за %d - second', $firstNumber, $secondNumber) : sprintf(' %d - second більше за %s - first', $secondNumber, $firstNumber);
echo '<pre>';
echo $roboto;
echo '</pre>';

echo '<pre> 3. Скорочення Імені та По батькові. Візьміть за основу своє ПІБ. У вас буде 3 змінні. (П.І.Б.) Ваша програма повинна скоротити І`мя та По батькові. ось приклад: (Іванов Іван Іванович)  (Іванов І. І.)</pre>';

$firstName = 'Спайк';
$lastName = 'Vinland';
$fatherName = 'ламія';
$name = sprintf('%s %s %s', $firstName, $lastName, $fatherName);
$subLastName = mb_strtoupper(substr($lastName, 0, 2), 'UTF-8');
$subFatherName = mb_strtoupper(substr($fatherName, 0, 2), 'UTF-8');
$result = sprintf('%s %s.%s.', $firstName, $subLastName, $subFatherName);
echo '<pre>';
echo sprintf(' %s => %s', $name, $result);
echo '</pre>';

echo '<pre> 4. Вам потрібно розробити програму, яка рахувала б кількість входжень якої-небудь обраної вами цифри в обраному вами числі. Наприклад: цифра 7 у числі 123456789 зустрічається 1 раз (обмежте себе функцією rand(1, 99999) - це ваше випадкове число) використовуй функцію substr_count()</pre>';

$math = rand(1, 9999);
echo sprintf(' Випадкове число %s ', $math);
$number = substr_count($math, 7);
echo '<pre>';
echo sprintf('Кількість входжень цифри 7 в %s дорівнює %s', $math, $number);
echo '</pre>';

echo '<pre>5. Переменные: </pre>';
echo '<pre>   a.  Створіть змінну $a і присвойте їй значення 3. Виведіть значення цієї змінної на екран.</pre>';
$a = 3;
echo '<pre>';
echo sprintf('змінна a = %s', $a);
echo '</pre>';
echo '<pre>  b. Створіть змінні $a=10 і $b=2. Виведіть на екран їхню суму, різницю, добуток і частку (результат ділення).</pre>';

$a = 10;
$b = 2;
$sum = sprintf('%s + %s ', $a, $b);
$difference = sprintf('%s - %s', $a, $b);
$extraction = sprintf('%s * %s', $a, $b);
$division = sprintf('%s / %s', $a, $b);
echo '<pre>';
echo sprintf('сума двох змінних %s', $sum) .'<br>';
echo sprintf('різниця двох змінних %s', $difference).'<br>';
echo sprintf('добуток двох змінних %s', $extraction).'<br>';
echo sprintf('результат ділення двох змінних %s', $division).'<br>';
echo $a . ' + ' . $b .' = ' . ($a + $b) . '<br>';
echo $a . ' - ' . $b .' = ' . ($a - $b) . '<br>';
echo $a . ' * ' . $b .' = ' . ($a * $b) . '<br>';
echo $a . ' / ' . $b .' = ' . ($a / $b);
echo '</pre>';

echo '<pre>  d. Створіть змінні $c=15 і $d=2. Підсумуйте їх, а результат присвойте змінній $result. Виведіть на екран значення змінної $result./pre>';
$c = 15;
$d = 2;
$result = $c + $d;

echo '<pre>  c. Створіть змінні $a=11, $b=7 і $c=5. Виведіть на екран їхню суму.</pre>';
$a = 11;
$b = 7;
$c = 5;

echo '<pre>  e. Створіть змінні $a=17 і $b=10. Відніміть від $a змінну $b і результат присвойте змінній $c. Потім створіть змінну $d, присвойте їй значення</pre>';

$a = 17;
$b = 10;
$c = $a - $b;
$d = $c;

echo '<pre>6. Складіть змінні $c і $d, а результат запишіть у змінну $result. Виведіть на екран значення змінної $result</pre>';

$result = $c + $d;


echo '<pre>7. Рядки: </pre>';
echo '<pre>  a. Cтворіть змінну $text і присвойте їй значення Привіт, Світ! Виведіть значення цієї змінної на екран.</pre>';
$string = 'hello world because I love stereotypes';

echo '<pre>  b. Створіть змінні $text1=Привіт,  і $text2=Світ!. За допомогою цих змінних і операції додавання рядків виведіть на екран фразу Привіт, Світ!..</pre>';
$strOne = 'hello world because';
$strTwo = ' I love stereotypes';
$result = $strOne . $strTwo;
echo '<pre>  c. Напишіть скрипт, який рахує кількість секунд у годині, добі, тижні, місяці з 30 днів.</pre>';
$hour = 60 * 60;
echo 'Кількість секунд в годині ' . $hour .' секунд'. '<br>';
$day = $hour * 24;
echo 'Кількість секунд у дні ' . $day .' секунд'. '<br>';
$weekday = $day * 7;
echo 'Кількість секунд у тижні ' . $weekday .' секунд'. '<br>';
$month = $weekday * 30;
echo 'Кількість секунд у місяці ' . $month . ' секунд';
echo '<pre>8. Переробіть наведений код так, щоб у ньому використовувалися операції +=, -=, *=, /=, ++, --. Кількість рядків коду при цьому не повинна змінитися! 
Код для переробки: </pre>';

$var = 1;
$var = $var + 12;
$var = $var - 14;
$var = $var * 25;
$var = $var / 2;
$var = $var % 7;
echo $var . '<br>';

$var = 1;
$var += 12;
$var -= 14;
$var *= 25;
$var /= 2;
$var %= 7;
echo $var . '<br>';
