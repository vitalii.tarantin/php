<?php

$url = strtolower($_SERVER['REQUEST_URI']);


if ($url == '/product') {
    include '../app/Controller/ProductController.php';
} elseif ($url == '/category') {
    include '../app/Controller/CategoryController.php';
} elseif ($url == '/order') {
    include '../app/Controller/Order.php';
} else {
    include '404.html';
}
