<?php
echo '<pre>Форма - завантаження файлів на сервер</pre>';
if (isset($_FILES['userfile'])) {
    if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {
        // Повертає TRUE, якщо файл filename був завантажений за допомогою HTTP POST
        $filename = basename($_FILES['userfile']['name']);
        // basename--Вертає ім'я файлу із зазначеного шлях
        $uploaddir = '/var/www/html/public/img/';
        $uploadfile = $uploaddir . $filename;
        move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile);
        echo "<img src='$uploadfile' alt='$filename' title='$filename' />";
    }
}
echo '<pre>Форма - завантаження файлів на сервер розширений варіант</pre>';

$max_image_width = 1280;
$max_image_height = 960;
$max_image_size = 960 * 1280;
$valid_types = array('png', 'jpeg', 'gif', 'jpg');

if (isset($_FILES['userfile'])) {
    if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {
        $filename = basename($_FILES['userfile']['name']);
        $ext = substr($_FILES['userfile']['name'], 1 + strrpos($_FILES['userfile']['name'], '.'));
        // strrpos - ПЯовертає позицію першого входження підрядка

        // отримаємо масив свойств файла
        $size = GetImageSize($_FILES['userfile']['tmp_name']);

        // перевіремо розмір фото
        if (filesize($_FILES['userfile']['tmp_name']) > $max_image_size) {
            echo 'Error: File size exceeds maximum allowed size.' . $max_image_size . '<br />';
        } elseif (!in_array($ext, $valid_types)) {
            echo 'Error: Invalid file type ';
        } elseif (($size) && ($size[0] < $max_image_width) && ($size[1] < $max_image_height)) {
            $uploaddir = '/var/www/html/public/img/';
            $uploadfile = $filename;
            move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile);
            echo "<img src='$uploadfile' alt='$filename' title='$filename' />";
        } else {
            echo 'Error: Invalid image properties ';
        }
    } else {
        echo 'Error: empty file';
    }
}
?>
<form enctype="multipart/form-data" method="post">
    Send this file: <input name="userfile" type="file">
    <input type="submit" value="Send File">
</form>
