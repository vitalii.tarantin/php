<?php
echo '<pre> BOOL TYPE </pre>';
$foo = true;

if ($foo) {
    echo 'I say DOU when my app print me error <br>';
}

echo '<pre> NUMBER TYPE </pre>';
$my_var = '0219';
var_dump((int)$my_var);


echo '<pre> FLOAT TYPE </pre>';
$testFloat = "52,2sdd";

echo (float)$testFloat . "<br>"; // отримаємо  52

$testFloat = "52.2sdd";
echo (float)$testFloat;
echo "<br>";

echo '<pre> STRING TYPE </pre>';
$str = 'I say DOO';
var_dump($str);
echo '<br>';

$str[strlen($str)+1] = 'U';
var_dump($str);

echo '<pre> ARRAY TYPE </pre>';

$array = ['homer', 'lisa', 'bart'];
$arrayOne = array('piter', 'brain', 'meg');
$arrayTwo = ['key1' => 'dima', 'key2' => 'Olia'];

echo '<pre>';
var_dump($array[2]);
echo '</pre>';

echo '<pre>';
print_r($arrayTwo['key2']);
echo '</pre>';

$arrayName = [
    'id'        => 1234,
    'age'       => 32,
    'firstName' => 'homer',
];
echo '<pre>';
print_r($arrayName);
echo '</pre>';

echo '<pre> OBJECT TYPE </pre>';

$res = (object) $arrayName;

echo '<pre>';
print_r($res);
echo '</pre>';


$obj = (object) 'qwerty';
echo $obj->scalar;

$id = 123;
echo "<p>id = $id</p>";
$id = "qwerty";
echo "<p>id = $id</p>";



echo '<pre> CONSTANT TYPE </pre>';

define('PI', 3.14);
$a = 2.34 * sin(3 * PI / 8) + 5;
echo $a;

$link = define('IMG', 'img');

echo '<pre> Logical expressions </pre>';
$less = 10 != 9;
var_dump($less);

$x =13;
$between = $x >= 8 && $x <= 17;
if ($between) {
    echo "<br>  $x in diapazon";
}
echo '<pre> incremental / decremental </pre>';

$a = 10;
$b = $a++;
echo "a=$a, b=$b";

echo '<pre> Ternary surgery </pre>';

$appraisal = 2;
echo $appraisal > 2 ? 'склав залік' : 'не склав залік <br>';



$obj = (object) 'qwerty';
echo '<pre>';
echo $obj->scalar;
echo '</pre>';




?>
<!--<img src="/--><?php //=IMG?><!--/images.jpg" alt="images">-->
