<?php

/**
 * @param string $str
 * @return string
 */
function getString(string $str): string {
    return $str;

}
print getString('i say dou when my app print me error');

/**
 * @param int $n
 * @return int
 */
function calcNumber(int $n): int {
    return $n**2;
}
$extent = calcNumber(6);
print '<pre>Параметри за замовчуванням</pre>';
/**
 * @param string $name
 * @param string $surname
 * @return string
 */
function getHello(string $name, string $surname = 'Calabanga' ): string {
    return "Привет $name $surname !!!";
}
$name = 'Nick';
print '<pre>Змінна кількість параметрів</pre>';
/**
 * @return void
 */
function myEcho(): void
{
    $count = func_num_args();
    for ($i = 0; $i < $count; $i++) {
        echo func_get_arg($i) . "<br>\n";
    }
}

/**
 * @param ...$args
 * @return void
 */
print '<pre>Змінна кількість параметрів ...args</pre>';
function tutorial(...$args): void
{
    print_r($args);
}
tutorial(11, 22, 33, 44);
print '<pre>Посилання php &</pre>';


//function testTime (&$array)
//{
//    for ($i = 0; $i < 100; $i++)
//    {
//        $array[$i];
//    }
//}
//
//$array = [];
//for ($i = 0; $i < 100; $i++)
//{
//    $array[] = $i;
//}
//
//$start = microtime(true);
//testTime($array);
//$time = microtime(true) - $start;
//
//print $time;

print '<pre>Передача параметрів за посиланням</pre>';

function changeColor(&$my_color) {
    $my_color = 'синий';
}
$color = 'красный';
changeColor($color);
echo $color;
print '<pre>Повернення за посиланням із функцій</pre>';
$numWidgets = 10;
function getNumWidgets() {
    global $numWidgets;
    return $numWidgets;
}
$Numwidgetsref = getNumWidgets();
$Numwidgetsref--;
echo " \$numWidgets = $numWidgets<br> ";
echo " \$numWidgetsRef = $Numwidgetsref<br> ";


function getahello($name) {
    echo "Hello, $name!";
    global $name;
    $name = 'Vasya';
}
getahello('Zack');
$name = 'Mike';

getahello($name);
echo $name;


print '<pre>';

print '</pre>';


function selfCount()
{
    static $count = 0;
    $count++;
    echo $count;
}
for ($i=0; $i < 5; $i++)
{
    selfCount();
}

selfCount();
selfCount();
selfCount();
echo '<br>';
echo print(print(print(print(123)))), print(4);

$array = [123, '454335', ['bar', 'var', [123, 'cvr', 45, [3565, null, function () {

}]], 'ere'], true];
echo '<pre>';
print_r($array);
echo '</pre>';
echo '<hr>';
//function qwerty(array $array)
//{
//    foreach ($array as $key => $value) {
//        if (gettype($value) == 'array') {
//            qwerty($value);
//        }
//        echo $key . ' => ' . $value . '<br>';
//    }
//    return $array;
//}
//
//echo '<pre>';
//qwerty($array);
//echo '</pre>';


print '<pre>Рекурсивні функції</pre>';
function factor($n) {
    static $count = 0;
    $count++;
    if($n <= 0) {
        print ($count) . ' static <br>';
        return 1;
    } else {
        return $n*factor($n-1);
    }
}

echo factor(5);

echo '<pre>My function dd</pre>';
echo '<hr>';
/**
 * @author Vitalii Tarantin <vitaliitarantin@gmail.com>
 * @param array $array
 * @return mixed
 */
function dd(array $array): mixed
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
    return true;
}
$array = [123, '454335', ['bar', 'var', [123, 'cvr', 45, [3565, null, function () {

}]], 'ere'], true];
dd($array);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="main.cssp">
    <title>Crash Test</title>
</head>
<body>
<h1><?php echo print 'PRINT FOUR SORT PHP'; ?></h1>
<p><?php echo  'PRINT FOUR SORT PHP'; ?></p>
<p><?= $extent; ?></p>
<p><?=
    getHello($name, 'Simpson');
    ?>
</p>
<p><?=
    print '<pre>Змінна кількість параметрів</pre>';
    myEcho("First sentence", "Second sentence", "Third sentence");
    ?>
</p>
<p><?=
    print '<pre>Змінна кількість параметрів ...args</pre>';
    tutorial(11, 22, 33, 44);
    ?>

</p>
</body>
</html>




