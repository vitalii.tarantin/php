<?php
function hyello($name, $h) {
    echo "<h$h>Hello, $name!</h$h>";
}

hyello('Mike', 1);
echo '<br>';
function myecho($a, $b) {
    for($i = 0; $i < func_num_args(); $i++) {
        echo func_get_arg($i) . "<br>\n";
    }
    //можно использовать foreach
}
//выведем отформатированные строки
myecho("First sentenc!!!", "Second sentenc!!!", "Third sentence!!!");

function test(...$args){
   echo '<pre>';
   print_r($args);
   echo '</pre>';
}
test(11, 22, 33, 44, 34, 42, 45, 67, 87, 45, 32324);

$a = &$b;
$b = 10; 								// теперь $b - то же самое, что и $a
$a = 3; 									// на самом деле $a = 0
echo "b = $b, a = $a";
echo '<br>';
$A = ['a' => 'aaa', 'b' => 'bbb'];
$b = &$A['b']; 	// теперь $b - то же, что и элемент с индексом 'b' массива
$b = 'bbbbbbbb'; 				// на самом деле $A['b']=0;
var_dump($A); 	// выводит 0
echo '<br>';
$A = ['a' => 'aaa', 'b' => 'bbb'];
$b = &$A['b'];

echo $A['b'];
echo '<br>';
function foo(&$my_color) {  		  	// теперь параметр будет ссылаться на оригинальное значение
    $my_color = 'синий';      			// присваиваем новое значени
    return $my_color;
}
$color = 'красный ';
foo($color);
print '<br>';
echo $color;// выведет: синий
print '<br>';
function funct(&$string)
{
    $string .= 'а эта внутри.';
}
$str = 'Эта строка за пределами, ';
funct($str);
echo $str;    											// Выведет 'Эта строка за пределами функции, а эта внутри.'
print '<br>';
$numWidgets = 10;
function &getNumWidgets() {
    global $numWidgets;
    return $numWidgets;
}
$Numwidgetsref = &getNumWidgets();
$Numwidgetsref++;
echo " \$numWidgets = $numWidgets<br> ";				// Выведет " 9 ".
echo " \$numWidgetsRef = $Numwidgetsref<br> ";

function func1() {
    function func2() {
        echo 'Я не существую пока не будет вызвана func1().\n';
    }
    echo 'Я существую всегда!!!';
}

//так как func2 еще не определена - её нельзя вызвать
func1();

//теперь можно вызывать fubc2
func2();



//function hello($name) {
//    echo "Hello, $name!";
//    global $name;
//    $name = 'Vasya';
//
//}
//print '<br>';
//hello('Zack');
//$name = 'Mike';
//print '<br>';
//hello($name);

			// Здесь уже будет Vasya
print '<br>';
function hello($name) {
    echo "Hello, $name!";
    $GLOBALS['name'] = 'Vasya';
}

hello('Zack');
$name = 'Mike';

hello($name);
echo $name;
// Здесь уже будет Vasya

print '<br>';


function selfcount() {
    static $count = 8;
    $count++;
    echo $count;
}

for($i = 0; $i < 5; $i++) {
    selfcount();
}
print '<br>';
function sum() {
    echo 2 + 2;
}
if (!function_exists('sum')) {
    function sum() {
        echo 2 + 2;
    }
} else {
    echo 'Такая функция уже есть!';
}
print '<br>';
function factor($n) {
    if($n <= 0) return 1;
    else return $n*factor($n-1);
}

echo factor(20);
print '<br>';
print_r(20);