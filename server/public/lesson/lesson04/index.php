<?php
echo '<pre>';
echo 'hello world because I love stereotypes';
echo '</pre>';


$a = rand(1,100);
$b = rand(1,100);
$c = $a > $b ? $a . ' - $a число больше чем $b - ' . $b : 'Число  - $b ' . $b . ' больше чем $a - ' . $a;
echo '<pre>';
echo $c;
echo '</pre>';

$a = array(1, 5, 8);
$b = array(3, 7, 2);
if($a > $b) {
    echo '158';
} elseif ($a < $b) {
    echo '372';
} else {
    echo 'none';
}

$test = 123;
$result = $test ?? 'this text';
// це ідентично цьому коду:
$result = isset($test) ? $test : 'this text';

echo '<br>';
$a = 123;
$b = 123;
$array = [9,4];
if($array) {
    echo 'a more b';
} else {
    echo 'b more a';
}
echo '<br>';
if ($a > $b) {
    echo 'a больше, чем b';
} elseif ($a == $b) {
    echo 'a равен b';
} else {
    echo 'a меньше, чем b';
}
echo '<br>';
echo '<pre> Цикл while </pre>';
$x=0;
//while ($x<15) {
//    $x++;
//    echo '<pre>';
//    var_dump($x);
//    echo '</pre>';
//}
echo '<br>';
echo '<pre> Цикл do while </pre>';
//$q = 1;
//do {
//    echo '<pre>';
//    var_dump($q);
//    echo '</pre>';
//} while ($q++<10);
echo '<pre> Цикл for </pre>';
for ($z=0; $z<10; $z++) echo ' |for - ' . $z;

echo $z;

echo '<br>';

$table = '<table border=1>';
for ($i = 1; $i <= 10; $i++) {
    $table .= '<tr>';
    for ($j = 1; $j <= 10; $j++) {
        $table .= '<td>' . $i * $j . '</td>';
    }
    $table .= '</tr>';
}
$table .= '</table>';
echo $table;

$w = 0;
$e = 0;
echo "<table border='1'";
for ($r = 0; $r < 256; $r += 51) {
    echo "<tr>";
    for ($w = 0; $w < 256; $w += 51) {
        for ($e = 0; $e < 256; $e += 51) {
            echo "<td style=background-color:rgb($r,$w,$e);>&nbps</td>";
        }
    }
}
echo "</tr>";
echo "</table>";
echo '<pre> Массиви </pre>';

$web = array('HTML', 'CSS', 'JavaScript', 'PHP', 'MySQL');
$count  = count($web);
echo $web[0] . '<br>'; // Виведе 'HTML'
echo $web[2] . '<br>'; // Виведе 'JavaScript'
echo $web[3]; // Виведе 'PHP'
echo '<pre> Ассоциативный массив</pre>';
$about = [
        [
            'name'			=> 'Nickolay',
            'age'			=> '34',
            'gender'	    => 'man',
            'profession'    => 'programmer',
            ]
    ];
foreach ($about as $key => $value) {
    echo $key . ' => ' . $value . '<br>';
    foreach ($value as $index => $item) {
        echo $index . ' => ' . $item . '<br>';
    }
}
echo '<pre> foreach </pre>';
$result = array (
    'Иванов' => ['рост' => 174, 'вес' => 68],
    'Петров' => ['рост' => 181, 'вес' => 90 ],
    'Волков' => ['рост' => 166, "вес"  => 73],
);

foreach ($result as $фамилия => $данные1)  {
    echo  "<br>$фамилия:<br>";
    foreach ($данные1 as $параметр => $pp) {
        echo  "$параметр = $pp<br>";
    }
}
echo '<pre>Реализовать свою функцию count(); Foreach </pre>';

$array = ['Alex', 'Kostya', 'Pavel', 'Oleg'];
$countForeach = 0;
foreach ($array as $item) {
    $countForeach++;
}
echo '<pre>';
echo $countForeach;
echo '</pre>';

echo '<pre>Реализовать свою функцию count(); While </pre>';
$countWhile = 0;
while ($array[$countWhile] != null) {
    $countWhile++;
}
echo '<pre>';
echo $countWhile;
echo '</pre>';

echo '<pre>Реализовать свою функцию count(); DoWhile </pre>';
$countDoWhile = 0;
do {
    $countDoWhile++;
}
while ($array[$countDoWhile] != null);
echo '<pre>';
echo $countDoWhile;
echo '</pre>';

echo '<pre>Реализовать свою функцию count(); For </pre>';
$countFor = 0;
for (; $array[$countFor] != null; $countFor++) {
}
echo '<pre>';
echo $countFor;
echo '</pre>';

echo '<pre>Собрать массив не четных чисел от 0 до 100  For </pre>';
$number = 0;
for (; $number <= 100; $number++) {
    if ($number % 2 != 0) {
        $arr[] = $number;
    }
}
echo '<pre>';
print_r($arr);
echo '</pre>';
echo '<pre>Собрать массив не четных чисел от 0 до 100  While </pre>';
$numberWhile = 0;
while ($numberWhile++ < 100 ) {
    if ($numberWhile % 2 != 0) {
        $arrWhile[] = $numberWhile;
    }
}
echo '<pre>';
print_r($arrWhile);
echo '</pre>';


?>

<!--<div>-->
<!--    --><?php //if ($a > $b): ?>
<!--    <div>-->
<!--        <p> asaswasgbrtbrtbrgbrgfb v rbrtgbretbvrtbrtgbdfb gr trn brtbnr3brtb</p>-->
<!--    </div>-->
<!--    --><?php //else: ?>
<!--    <div>-->
<!--        <ul>-->
<!--            <li>123</li>-->
<!--            <li>456</li>-->
<!--            <li>789</li>-->
<!--        </ul>-->
<!--    </div>-->
<!--    --><?php //endif; ?>
<!--</div>-->
<ul>
    <?php while ($x++ < 10 ): ?>
    <li>
        <?= $x ?>
    </li>
    <?php endwhile; ?>
</ul>
<form action="">
    <select name="lang">
        <?php while (--$count >= 0): ?>
        <option value="<?= $web[$count]?>">
            <?= $web[$count]?>
        </option>
        <?php endwhile; ?>
    </select>
</form>