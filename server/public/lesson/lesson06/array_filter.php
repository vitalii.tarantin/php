<?php
/**
 * @param $array
 * @param $callback
 * @return array
 * @author Vitalii Tarantin <vitaliitarantin@gmail.com>
 */
function myFilter($array, $callback): array
{
    $result = [];
    foreach ($array as  $value) {
        if ($callback($value) != 'false') {
            $result[] = $callback($value);
        }
    }
    return $result;
}

$array = ['Arsen', 'Tolya', 'Kolya', 'Andrii', 'Vitalii', 'Mark', "Vlad"];

//$result = array_filter($array, function ($value) {
//    if ($value[0] == 'V') {
//        return $value;
//    }
//});
$res = myFilter($array, function($val) {
    if ($val[0] == 'V') {
        return $val;
    } else {
        return 'false';
    }
});
print '<pre>';
print_r($res);
print '</pre>';