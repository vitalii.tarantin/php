<?php
function getUnit($one, $few, $many, $count = 1) {
    if ($count % 10 == 1 && $count % 100 != 11) {
        $output = $one;
    } elseif (($count % 100 < 10 || $count % 100 >= 20) && $count % 10 >= 2 && $count % 10 <= 4) {
        $output = $few;
    } else {
        $output = $many;
    }
    return $output;
}