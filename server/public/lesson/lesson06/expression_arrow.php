<?php
$start = microtime(true);


//function declarationRectangleArea($a, $b) {
//    return $a * $b;
//}
//echo declaration(3,4) . '<br>';

//$expressionRectangleArea = function($a, $b) {
//    return $a * $b;
//};
//echo $expression(3,4) . '<br>';

//$arrowRectangleArea = fn($a, $b) => $a * $b;
//echo $arrowRectangleArea(21,21) . '<br>';

$fh1 = fn($a,$b,$c) => ($a > $b && $a > $c) ? $a : (( $b> $a && $b > $c) ? $b : $c);
echo $fh1(7,3,4);

$time = microtime(true) - $start;
echo $time;