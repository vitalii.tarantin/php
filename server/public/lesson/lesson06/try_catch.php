<?php

function df($MySTR, $FILE = 'MyLog.txt')
{
    $date = date("Y-m-d G:i:s");
    $fp = fopen($_SERVER["DOCUMENT_ROOT"] . $FILE, 'a+');
    $str = $date . " " . print_r($MySTR,true). "\r\n";
    fwrite($fp, $str);
    fclose($fp);
}

function inverse($x) {
    if (!$x) {
        throw new Exception('error message 404 ' . include_once '404.html');
    }
    return 1 / $x;
}

try {
    echo inverse(5) . "\n";
    echo inverse(0) . "\n";
} catch (Exception $exception) {
    df( $exception->getMessage());
}