SELECT * FROM school_db.Students;

update Students set
                    enrollment_date = '1998-04-13',
                    last_name = 'Eddy',
                    first_name = 'Brok'
where id = 6;

-- Update all enrollment dates that are enpty (null)
update Students set
    enrollment_date = '1996-12-12'
where enrollment_date is null;

-- Update all enrollment dates
-- Update multiple records (be careful)
update Students set enrollment_date = '2002-02-02';

-- Update students  with ids 7 & 11. Change first and last names
-- Update multiple columns
update Students set
                    last_name = 'Ricardo',
                    first_name = 'Pizzinolla'
where id = 9;

/*
Update Students set firstname = 'Rhoddy', lastname = 'Shawn' where id in (4, 10);
-- The IN keyword will search for records that meet the condition of the values specified in the given list
*/
update Students set
                    last_name = 'Lui',
                    first_name = 'Paster'
where id IN (4, 10);