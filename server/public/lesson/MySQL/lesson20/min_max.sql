/*
	Bring back the highest and lowest grades for each course
*/
select class_id as `Class`, min(grade) as `Minimum Grade`, max(grade) as `Maximum Grade` from enrollments
group by class_id