SELECT * FROM school_db.Students;
-- Retrieve only students with the first name 'Simpson'
select last_name, first_name from school_db.Students where last_name = 'Homer';

-- Retrieve only students with the word 'Student' in their last name % - add this modules
-- % alphabet symbol % - find word with this symbol
-- %s - find wor ends s
-- s% find word begin s
select last_name, first_name from school_db.Students where first_name like 'Doe%';

-- Retrieve only Full Names of Students and their enrollment date
select concat(last_name, ' ', first_name) `full name`, enrollment_date `Enrollment Date` from school_db.Students;

-- Select Courses with the number of credits greater than 2
select * from school_db.Courses where number_of_credits > 2;

-- Select Courses with the number of credits 3 and less
select * from school_db.Courses where number_of_credits <= 3;