-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema amazon_clone
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema amazon_clone
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `amazon_clone` ;
USE `amazon_clone` ;

-- -----------------------------------------------------
-- Table `amazon_clone`.`custoners`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amazon_clone`.`custoners` (
                                                          `id` INT NOT NULL AUTO_INCREMENT,
                                                          `email_address` VARCHAR(45) NOT NULL,
    `full_name` VARCHAR(45) NULL,
    PRIMARY KEY (`id`))
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = binary;


-- -----------------------------------------------------
-- Table `amazon_clone`.`orders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amazon_clone`.`orders` (
                                                       `id` INT NOT NULL,
                                                       `order_number` VARCHAR(45) NOT NULL,
    `order_date` DATETIME NULL,
    `customer_id` VARCHAR(45) NULL,
    `custoners_id` INT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_orders_custoners1_idx` (`custoners_id` ASC),
    CONSTRAINT `fk_orders_custoners1`
    FOREIGN KEY (`custoners_id`)
    REFERENCES `amazon_clone`.`custoners` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `amazon_clone`.`products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amazon_clone`.`products` (
                                                         `id` INT NOT NULL,
                                                         `code` VARCHAR(45) NULL,
    `name` VARCHAR(255) NULL,
    `description` VARCHAR(255) NULL,
    PRIMARY KEY (`id`))
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `amazon_clone`.`products_orders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amazon_clone`.`products_orders` (
                                                                `id` INT NOT NULL AUTO_INCREMENT,
                                                                `orders_id` INT NOT NULL,
                                                                `products_id` INT NOT NULL,
                                                                PRIMARY KEY (`id`),
    INDEX `fk_products_orders_orders_idx` (`orders_id` ASC),
    INDEX `fk_products_orders_products1_idx` (`products_id` ASC),
    CONSTRAINT `fk_products_orders_orders`
    FOREIGN KEY (`orders_id`)
    REFERENCES `amazon_clone`.`orders` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_products_orders_products1`
    FOREIGN KEY (`products_id`)
    REFERENCES `amazon_clone`.`products` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
