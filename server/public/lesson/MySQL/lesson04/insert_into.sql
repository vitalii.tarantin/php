-- INSERT INTO TaleName(Columns...) Value(values...)
-- Single Row Insert
insert into Students
(last_name, first_name,date_of_birth,enrollment_date)
values
    ('Jon', 'Doe','1978-01-12','2002-11-13');

-- INSERT INTO TaleName(Columns...) Value(values...), (values...), (values...), (values...);
-- Multiple Row Insert
insert into Students
(last_name, first_name,date_of_birth,enrollment_date)
values
    ('Jon', 'Doe','1978-01-12','2002-11-13'),
    ('Jon2', 'Doe2','1979-01-12','2002-11-13'),
    ('Jon3', 'Doe3','1980-01-12','2002-11-13'),
    ('Jon4', 'Doe4','1981-01-12','2002-11-13'),
    ('Jon5', 'Doe5','1982-01-12','2002-11-13'),
    ('Jon6', 'Doe6','1983-01-12','2002-11-13'),
    ('Jon7', 'Doe7','1984-01-12','2002-11-13'),
    ('Jon8', 'Doe8','1985-01-12','2002-11-13'),
    ('Jon8', 'Doe8','1986-01-12','2002-11-13'),
    ('Jon8', 'Doe8','1987-01-12','2002-11-13'),
    ('Jon9', 'Doe9','1988-01-12','2002-11-13'),
    ('Jon10', 'Doe10','1989-01-12','2002-11-13');