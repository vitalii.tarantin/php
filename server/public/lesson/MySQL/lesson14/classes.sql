CREATE TABLE `school_db`.`classes` (
                                       `id` INT NOT NULL AUTO_INCREMENT,
                                       `lecture_id` INT NULL,
                                       `courses_id` INT NOT NULL,
                                       `time` TIME NOT NULL,
                                       PRIMARY KEY (`id`),
                                       INDEX `classes_lecrure_idx` (`lecture_id` ASC),
                                       INDEX `classes_courses_idx` (`courses_id` ASC),
                                       CONSTRAINT `classes_lecrure`
                                           FOREIGN KEY (`lecture_id`)
                                               REFERENCES `school_db`.`lectures` (`id_lectures`)
                                               ON DELETE NO ACTION
                                               ON UPDATE NO ACTION,
                                       CONSTRAINT `classes_courses`
                                           FOREIGN KEY (`courses_id`)
                                               REFERENCES `school_db`.`Courses` (`course_id`)
                                               ON DELETE NO ACTION
                                               ON UPDATE NO ACTION);