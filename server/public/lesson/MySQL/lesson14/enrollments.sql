CREATE TABLE `school_db`.`enrollments` (
                                           `id` INT NOT NULL AUTO_INCREMENT,
                                           `class_id` INT NOT NULL,
                                           `students_id` INT NOT NULL,
                                           PRIMARY KEY (`id`),
                                           INDEX `enrollments_students_idx` (`students_id` ASC),
                                           INDEX `enrollments_class_idx` (`class_id` ASC),
                                           CONSTRAINT `enrollments_students`
                                               FOREIGN KEY (`students_id`)
                                                   REFERENCES `school_db`.`Students` (`id`)
                                                   ON DELETE NO ACTION
                                                   ON UPDATE NO ACTION,
                                           CONSTRAINT `enrollments_class`
                                               FOREIGN KEY (`class_id`)
                                                   REFERENCES `school_db`.`classes` (`id`)
                                                   ON DELETE NO ACTION
                                                   ON UPDATE NO ACTION);
