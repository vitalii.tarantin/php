-- Select all scheduled classes to which a lecturer has been assigned

SELECT * FROM school_db.classes class
                  inner join school_db.lectures lect on class.lecture_id = lect.id_lectures
                  inner join school_db.Courses cour on class.courses_id = cour.course_id;

-- Select concat scheduled classes to which a lecturer has been assigned

SELECT
    concat(lect.last_name, ', ', lect.first_name) as `Lecturer Full Name`,
    cour.title as `Course Title`,
    cour.number_of_credits as `Number of Credits`
FROM school_db.classes class
         inner join school_db.lectures lect on class.lecture_id = lect.id_lectures
         inner join school_db.Courses cour on class.courses_id = cour.course_id;

-- Select the details of each enrollment by tarting the Student Name, the course, the lecturer and the time of the class session

SELECT
    concat(lect.last_name, ', ', lect.first_name) as `Lecturer Full Name`,
    concat(stu.last_name, ', ', stu.first_name) as `Students Full Name`,
    cour.title as `Course Title`,
    class.time as `Session Time`
FROM school_db.enrollments enrols
         inner join school_db.classes class on class.id = enrols.class_id
         inner join school_db.Students stu on stu.id = enrols.students_id
         inner join school_db.lectures lect on class.lecture_id = lect.id_lectures
         inner join school_db.Courses cour on class.courses_id = cour.course_id;