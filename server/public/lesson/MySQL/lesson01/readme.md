# IN THE COMMAND LINE USE ONLY UPPERCASE

### This command is creating a database
> CREATE DATABASE school_db;

### This command is show table in the choose database
> select * from your_database_name.your_table_name;

### This command is show table where id = 2 in the choose database

> select * from your_database_name.your_table_name where id = 2;

### USE database

> USE your_database_name