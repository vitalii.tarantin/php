/*
	Bring back the number of students enrolled per class
*/
use school_db;
select class_id, count(students_id) from enrollments
                                             inner join classes c on class_id = c.id
                                             inner join Courses co on co.course_id = c.courses_id
group by  class_id;