-- Remove one row by ID
delete from Students
where id = 9;

-- Remove multiple rows using a list of potential values
delete from Students
where id IN(5,7);

-- Delete student named Lui Paster
delete from Students
where last_name = 'Lui' and first_name = 'Paster';

-- Delete all students with names containing the word  'student'
delete from Students
where first_name like '%student%' or last_name like '5students%';