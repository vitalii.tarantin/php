/*
	Bring back the total number of credits that each student is doing
*/
use school_db;

select
    concat(s.last_name, ', ', s.first_name) as `Students Name`,
    sum(co.number_of_credits) as `Total Credits`
from enrollments e
         inner join Students s on e.students_id = s.id
         inner join classes c on e.class_id = c.id
         inner join Courses co on c.courses_id = co.course_id
group by s.last_name, s.first_name