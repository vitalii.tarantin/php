### Start project
>docker-compose up
> 
>docker-compose up -d
> 
>docker-compose up --build

### Stop project
>docker-compose stop

>docker-compose down

### ⚠️ Kill project ⚠️
>docker-compose kill
> 
>docker-compose down --rm=all

### ⚠️ Kill volume if you want reset password and if you want delete history of this volume ⚠️
>sudo find / -name "*name-db"
> 
>docker volume prune

### Docker start mysql
>docker exec -it my_mysql /bin/bash
> 
>mysql -h 127.0.0.1 -u root -p

### WEB
> http://localhost:8181

### Docker bash
> docker exec -it my_php /bin/bash

### Problem private
> sudo chown -R $USER server


### PHPUnit
> composer dump-autoload -o

> ./vendor/bin/phpunit --testdox


### PHP pre-commit
>./vendor/bin/phpcs -h
> 
>phpcs --standard=./phpcs.xml ./public/index.php
> 
> ./vendor/bin/phpcs --standard=./phpcs.xml ./public/index.php
> 
>phpcs --standard=PSR12 ./public/index.php
> 
>phpcs --standard=PEAR ./public/index.php



